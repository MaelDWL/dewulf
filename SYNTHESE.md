---

# Projet RASSEMBLEMENT

L’objectif est de réaliser une application web mobile, tout en ayant une liberté créative sur le sujet principal sur le thème Marvel en utilisant l’api (developer.marvel.com).Nous travaillons en groupe, en mode projet pour concevoir grace à une API, une application servant à collecter des éléments de cette API et les afficher. Elle va permettre de retrouver tous les éléments du thème choisi (personnages, BD), et les détaille lui concernant.

## Description de la stack tcehnique

* Front-end : HTML/CSS/JavaScript/VueJs
* Back-end : PHP
* Desktop : VsCode/Wamp

## Environnement local

### 🌐 Serveur web

* Nom : Apache
* Version : 
* Port HTTP : 80
* Port HTTPS : 443
* Chemin de l'exécutable : 
* Chemin du fichier de configuration : ../wamp64/www/apimarvel

Démarrage du serveur :

    $ npm run dev

Extinction du serveur :

    $ (ligne de commande ou chemin vers l'exécutable)

### 💽 Base de données

* Nom : API Marvel 
* Accès manuel : /          
* Version : /
* Port : /
* Chemin de l'exécutable : /
* Chemin du fichier de configuration : /
* Utilisateur : /
* Mot de passe : /

Démarrage de la base de données :

    $ (ligne de commande ou chemin vers l'exécutable)

Extinction de la base de données :

    $ (ligne de commande ou chemin vers l'exécutable)

## Environnement de production

* Hébergeur : Alwaysdata
* Type d'hébergement : dédié
* Datacenter : Datacenter Equinix à Paris
* Pays du datacenter : France
* Adresse IPv4 : /
* Adresse IPv6 : /
* Nom de domaine lié : /
* Sauvegarde automatique :non

Le code source du projet se trouve ici :

    D:\Code\Projets\Hubert\Marvel\marvelapi\RASSEMBLEMENT

Le projet est accessible ici :

    (URLs publiques)

---

---

# Projet NAVY-MARINE

Navy Marine est une compagnie privée de navigation maritime française assurant des liaisons maritimes par bateau depuis les ports continentaux français vers de multiples destinations à travers le monde.
Dans le contexte d’évolution de notre compagnie de navigation maritime, nous avons le souhait de doter nos services d’un outil de qualité. L’objectif de ce projet est de développer une application métier desktop ayant pour but de faciliter et numériser la gestion métier des activités de l’entreprise. En effet, cette gestion métier fonctionne uniquement au format papier et cela devient très contraignant au vu du nombre important de voyageurs voulant profiter de nos services.

## Description de la stack tcehnique

* Front-end : FXML/HTML/CSS
* Back-end : SQL/Java/JavaFX
* Desktop : VsCode/Wamp/SceneBuilder/Intellij

## Environnement local

### 🌐 Serveur web

* Nom : Wamp
* Version : 3.2.6
* Port HTTPS : 80   
* Port HTTP : /
* Chemin de l'exécutable : /
* Chemin du fichier de configuration : ../wamp64/www/

### 💽 Base de données

* Nom : MySQL
* Accès manuel : Adminer      
* Version : 4.8.1
* Port : 3306
* Chemin de l'exécutable : /
* Chemin du fichier de configuration : /
* Utilisateur : root
* Mot de passe : root

Démarrage de la base de données :

    $ C:\wamp64\www

Extinction de la base de données :

    $ (ligne de commande ou chemin vers l'exécutable)

## Environnement de production

* Hébergeur : Alwaysdata
* Type d'hébergement : dédié
* Datacenter : Datacenter Equinix à Paris
* Pays du datacenter : France
* Adresse IPv4 : /
* Adresse IPv6 : /
* Nom de domaine lié : /
* Sauvegarde automatique :non

Le code source du projet se trouve ici :

    D:\Code\Projets\MVB Abdelmoula

Le projet est accessible ici :

    (URLs publiques)

---

