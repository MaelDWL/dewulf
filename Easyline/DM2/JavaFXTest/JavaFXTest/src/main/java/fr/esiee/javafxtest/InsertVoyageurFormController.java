package fr.esiee.javafxtest;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;

import java.io.ObjectInputStream;

public class InsertVoyageurFormController {

    TextField textField = new TextField("tfNom");

    public void validerForm(ActionEvent actionEvent) {
        System.out.println("Le formulaire a bien été validé !");
        System.out.println("");
        System.out.println("Voici les informations saisies :");
        System.out.println();
    }

    public void annulerForm(ActionEvent actionEvent) {
        textField.clear();
        System.out.println("Le formulaire a bien été annulé !");

    }
}
