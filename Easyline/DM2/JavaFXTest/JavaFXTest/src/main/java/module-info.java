module fr.esiee.javafxtest {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens fr.esiee.javafxtest to javafx.fxml;
    exports fr.esiee.javafxtest;
}