package fr.esiee.javafxtest;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

//on a un controller par pannel fxml
//Gestion d'événement géréé ici (click, validation,passage de souris)

public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {

        welcomeText.setText("Welcome to my Application!");

        System.out.println("j'ai cliquer");
    }
    //méthode dans controller utlisé ici
}