package fr.esiee.javafxtest;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

//application que l'on doit démarrer  (cette classe doit hériter d'application
// ici on charge le premier FXML qui sera le panel root de l'application
public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        //on charge le FXML
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("insertVoyageurForm-view.fxml"));
        // load permet de charger le panel décrit dans le FXML et le controller associé
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        //ici on affiche la fenetre
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}