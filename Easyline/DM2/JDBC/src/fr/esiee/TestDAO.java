package fr.esiee;

import fr.esiee.model.AdressePostale;
import fr.esiee.dao.VoyageurDAO;
import fr.esiee.model.Voyageur;

import java.sql.SQLException;

public class TestDAO {
    public static void main(String [] args) throws SQLException {
        // Pour tester la méthode que nous avons ajouté
        // Pour test insertAdresse il faut un objet VoyageurDAO et un objet AdressePostale
        VoyageurDAO vDAO = new VoyageurDAO();
        AdressePostale al = new AdressePostale("3 résidence de Cergy", "Cergy", "95");
        // Tester le service avec la nouvelle adresse
        int iAdresse = vDAO.insertAdresse(al);
        System.out.println("adresse ajoutée : " + al);
        System.out.println("id de la nouvelle adresse : " + vDAO.idAdresse());

    }
}
