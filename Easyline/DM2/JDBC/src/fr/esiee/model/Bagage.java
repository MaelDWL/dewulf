package fr.esiee.model;

public class Bagage {
	
	private int numero;
	private String couleur;
	private double poids;

	private String Gabarit;

	// Initialisation d'un constructeur à 0 argument
	public Bagage(){

	}

	// Initialisation d'un constructeur à 3 arguments
	public Bagage(int numero, String couleur, double poids)
	{
		setNumero(numero);
		setCouleur(couleur);
		setPoids(poids);
		setGabarit(gabarit)
	}

	// Initialisation des Getters
	int getNumero()
	{
		return this.numero;
	}
	String getCouleur()
	{
		return this.couleur;
	}
	double getPoids()
	{
		return this.poids;
	}

	String getGabarit()
	{
		return this.gabarit;
	}

	String getGabarit

	// Initialisation des Setters
	public void setNumero(int numero)
	{
		this.numero = numero;
	}
	public void setCouleur(String couleur)
	{
		this.couleur = couleur;
	}
	public void setPoids(double poids)
	{
		this.poids = poids;
	}

	// Initialisation de la méthode afficher pour l'affichage du bagage
	private void afficher()
	{
		System.out.println(
			"\n|- Bagage -| \n" + this
		);
	}

	// Initialisation du Override pour les carastéristique du bagage
	@Override
	public String toString()
	{
		return "\n|-> Numéro : " + getNumero() + "\n|-> Couleur : " + getCouleur()+ "\n|-> Poids : " + getPoids() + " Kg";
	}

	public static void main(String[] args)
	{
		// Création d'un bagage
		Bagage bagage = new Bagage(95, "bleu", 23);
		bagage.afficher();

		// Modification du bagage avec le méthodes SET et le réaffiche
		bagage.setNumero(972);
		bagage.setCouleur("jaune");
		bagage.setPoids(10);
		bagage.afficher();
		System.out.println("\n");
	}
}
