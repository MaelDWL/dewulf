package fr.esiee.model;

import java.util.ArrayList;
import java.util.Scanner;

public class Voyageur {
	private String nom;
	private String prenom;
	private int age;
	private String category;
	private AdressePostale adressePostale;
	private Bagage bagages;

	// Initialisation d'un constructeur à 0 argument
	Voyageur() {
	}

	// Initialisation d'un constructeur à 2 arguments
	Voyageur(String nom, int age) {
		setNom(nom);
		setAge(age);
	}

	// Initialisation d'un constructeur à 4 arguments
	Voyageur(String nom, int age, AdressePostale adressePostale, Bagage bagages) {
		setNom(nom);
		setAge(age);
		setAdressePostale(adressePostale);
		setBagage(bagages);

	}

	// Initialisation des Getters
	public String getNom() {

		return this.nom;
	}

	public String getPrenom() {

		return this.prenom;
	}

	public int getAge() {

		return this.age;
	}

	public String getCategory() {

		return this.category;
	}

	public AdressePostale getAdressePostale() {

		return this.adressePostale;
	}

	public Bagage getBagages() {

		return this.bagages;
	}

	// Initialisation des Setters

	public void setNom(String nom) {
		Scanner sc = new Scanner(System.in);

		System.out.println("\n| Votre voyageur | \n" + "| Veuillez saisir votre nom ! ");
		nom = sc.nextLine();
		this.nom = nom;
	}
	public void setPrenom(String prenom) {
		Scanner sc = new Scanner(System.in);

		System.out.println("\n| Votre voyageur | \n" + "| Veuillez saisir votre prénom ! ");
		prenom = sc.nextLine();
		this.prenom = prenom;
	}

	public void setAge(int age) {
		Scanner sc = new Scanner(System.in);

		System.out.println("\nVeuillez saisir votre âge ! ");
		age = sc.nextInt();

		while (age < 0) {
			System.out.println("! Veuillez saisir un age réel !");
		}
		this.age = age;
		setCategory(age);
	}

	public void setCategory(int age) {

		if (age < 2) {
			System.out.println("Nourrisson");
		} else if (age < 19) {
			System.out.println("Enfant");
		} else if (age < 61) {
			System.out.println("Adulte");
		} else {
			System.out.println("Senior");
		}

	}

	public void setAdressePostale(AdressePostale adressePostale) {
		this.adressePostale = adressePostale;
	}

	public void setAdressePostale(String voie, String ville, String codePostal) {
		this.adressePostale = new AdressePostale(voie, ville, codePostal);
	}

	public void setAdressePostale() {
		Scanner sc = new Scanner(System.in);

		System.out.println("|-- Adresse Postale --| ");

		System.out.println("\n| Quelle est votre voie ?");
		String voie = sc.nextLine();
		System.out.println("\n |Quelle est votre ville ?");
		String ville = sc.nextLine();
		System.out.println("\n| Quelle est votre code postal ?");
		String codePostal = sc.nextLine();

		this.adressePostale = new AdressePostale(voie, ville, codePostal);

	}

	private void setBagage(Bagage bagages) {
		this.bagages = bagages;
	}

	public void setBagages() {
		Scanner sc = new Scanner(System.in);

		System.out.println("|-- Bagage --|");

		System.out.println("\n| Combien de bagage avez vous ?");
		int bagage = sc.nextInt();

		for (int i = 0; i < bagage; i++) {
			System.out.println("\n| Bagage n° " + (i + bagage));

			System.out.println("\n| Quel est le numéro de votre bagage ?");
			int numero = sc.nextInt();
			System.out.println("\n| De quelle couleur est votre bagage ?");
			String couleur = sc.next();
			System.out.println("\n| Combien pèse votre bagage ?");
			double poids = sc.nextInt();

			this.bagages = new Bagage(numero, couleur, poids);
		}
	}

	public void afficher() {
		System.out.println(
				"\n\n| Carte du voyageur |\n" + this + "\n\n");
	}

	@Override
	public String toString() {
		int i = 1;
		String msg = "|- Nom : " + getNom() +"\n|- Prénom : " + getPrenom() + "\n|- Age : " + getAge() + "\n|- Catégorie : " + getCategory() + (getAdressePostale() != null ? ("\n|- Adresse postale : " + getAdressePostale()) : "") + "\n|- Bagage : " + getBagages();

		return msg;
	}

}
