package fr.esiee.dao;

import fr.esiee.model.AdressePostale;
import fr.esiee.model.Bagage;
import fr.esiee.model.Voyageur;

import java.sql.*;

public class VoyageurDAO {
    private static final String urlBDD = "jdbc:mysql://localhost:3306/easyline";
    //TODO ajouté le lien vers BDD déployé sur internet

    private static final String user = "root";
    private static final String pwd = "root";

    public int idBagage(){
        return 0;
    }

    public int insertBagage(Bagage ap) throws SQLException {
        String reqInsertAdress = "INSERT INTO Bagage (couleur, poid, numero, gabarit) VALUES (?,?,?,?)";
        int geneId = 0;
        Connection conn = null;
        try{
            // Récupération d'un objet de type Connexion : son nom "conn"
            conn = DriverManager.getConnection(urlBDD, user, pwd); // Nécessite le driver dans le classpath
            System.out.println("Connection OK.");

            PreparedStatement statement = conn.prepareStatement(reqInsertAdress, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, ap.getCouleur());
            statement.setString(2, ap.getPoids());
            statement.setString(3, ap.getNumero());
            statement.setString(3, ap.getGabarit());
            int rows = statement.executeUpdate();
            if (rows > 0){
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()){
                    geneId = rs.getInt(1);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally { //ce bloc s'execute en cas d'erreur ou pas !
            //TODO fermer connection TJRS
            if(conn != null) {
                conn.close();
            }
        }
        return geneId;
    }

    public int insertVoyageur(Voyageur v) throws SQLException{
        //TODO insérer l'adresse du voyageur et récupérer l'ID de l'adresse

        int idAdresse = 0;
        String reqInsertVoyageur = "INSERT INTO voyageurs (nom, prenom, anneeNaiss, adresse) VALUES (?, ?, ?, ?)";
        int geneId = 0;
        try{
            Connection conn = DriverManager.getConnection(urlBDD, user, pwd); // Nécessite le driver dans le classpath
            System.out.println("Connection OK.");

            PreparedStatement statement = conn.prepareStatement(reqInsertVoyageur, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, v.getNom());
            statement.setString(2, v.getPrenom());
            statement.setInt(3, v.getAge());
            statement.setInt(4, idAdresse());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return idAdresse;
    }

}
