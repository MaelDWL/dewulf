import java.util.*;             //question1 2 et 3

public class Voyageur {
    Scanner sc = new Scanner(System.in);
    private String nom;
    private int age;
    private String cat;

    //constructeur 1
    public Voyageur(String nom, int age, String cat) {
        this.nom=nom;
        this.cat=cat;
        this.age=age;
    }


    public Voyageur() {
    }








    public void afficher(){

        System.out.println(this.nom + " " + this.age);



    }

    public String toString(){

        return "|Voyageur : " + this.nom + "| |" + this.age + " ans| " + this.cat;

    }

    public String getNom(){

        return nom;

    }

    public int getAge(){

        return age; 

    }

    public int SetAge(){ 
        Scanner as = new Scanner(System.in);
        int ageA = as.nextInt();        
        while(ageA<0){ 
            System.out.println("Age incorrect,Veuillez réessayer.");
            ageA = as.nextInt();
        }
        this.age = ageA;
        as.close();
        return age;
    }

    public String SetNom(){ 
        Scanner as = new Scanner(System.in);
        String nomb = as.nextLine();        
        while((nomb.length())<2){ 
            System.out.println("Nom incorrect, Veuillez réessayer.");
            nomb = as.nextLine();
        }
        this.nom = nomb;

        return nom;
    }

    static String setCat(int age){
        String cat = "";
        if(age<1){
            cat="|Nourrison|";
        }
        
        else if(age>1 && age<18){
            cat="|Enfant|";
        }
        
        else if(age>=18 && age<60){
            cat="|Adulte|";
        }

        else{
            cat="|Sénior|";
        }
        return cat;
    }   


    

     //constructeur 2




    public static void main(String args[]){



        //creation d'un objet // v.afficher();
 
        Voyageur v=new Voyageur  ("|Dupont|", 54, "");
        v.cat = setCat(v.age);
        System.out.println(v);

           //constructeur du voyageur 

       Voyageur v2=new Voyageur();
        
       Scanner sc = new Scanner(System.in);

        System.out.println("Quel voyageur êtes-vous ? ");          
        System.out.println("Nom :");
       
        v2.SetNom();
        System.out.println("Age :");
        v2.SetAge();
        v2.cat = setCat(v2.age);
        System.out.println(v2);
        sc.close();
    }


}
