public class Main {
    public static void main(String[] args) {
        AdressePostale adresse  = new AdressePostale("70 Allée des Frères Lumière", "Bussy-Saint-Georges", 77600);
        Voyageur voyageur       = new Voyageur("Luc", adresse);

        System.out.println("--- VOYAGEUR ---");
        System.out.println(voyageur);

        voyageur.setAdresse("29 Rue des Boulets", "Paris", 75011);

        System.out.println("--- VOYAGEUR ---");
        System.out.println(voyageur);

        voyageur.setAdresseVille("Paris");

        System.out.println("--- VOYAGEUR ---");
        System.out.println(voyageur);

        voyageur.ajouteBagage(437, "Noir", 37);

        System.out.println("--- VOYAGEUR ---");
        System.out.println(voyageur);
    }
}
