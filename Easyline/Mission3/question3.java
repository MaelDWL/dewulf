public class Bagage
{
	private int 	numero;
	private int		poids;
	private String 	couleur;


	/* CONSTRUCTEUR */
	public Bagage(int p_numero, String p_couleur, int p_poids)
	{
		this.numero     = p_numero;
		this.couleur    = p_couleur;
		this.poids		= p_poids;
	}

	/* OVERRIDE */
	@Override
	public String toString()
	{
		return "> Bagage\n" +
				"    Numéro  :       " + this.numero + "\n" +
				"    Couleur :       " + this.couleur + "\n" +
				"    Poids   :       " + this.poids + " kg";
	}

	/* SETTER */
	/* Mets à jour la propriété < numero > de la classe < Bagage > */
	public void setNumero(int p_numero)
	{
		this.numero = p_numero;
	}

	/* Mets à jour la propriété < couleur > de la classe < Bagage > */
	public void setCouleur(String p_couleur)
	{
		this.couleur = p_couleur;
	}

	/* Mets à jour la propriété < poids > de la classe < Bagage > */
	public void setPoids(int p_poids)
	{
		this.poids = p_poids;
	}
}
