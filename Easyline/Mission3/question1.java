public class AdressePostale
{
    private int     codePostal;
    private String  voie;
    private String  ville;


    /* CONSTRUCTEUR */
    public AdressePostale(String p_voie, String p_ville, int p_codePostal)
    {
        this.voie           = p_voie;
        this.ville          = p_ville;
        this.codePostal     = p_codePostal;
    }

    /* OVERRIDE */
    @Override
    public String toString()
    {
        return "> Adresse\n" +
            "    Voie :          " + this.voie + "\n" +
            "    Ville :         " + this.ville + "\n" +
            "    Code postal :   " + this.codePostal;
    }

    /* SETTER */
    /* Mets à jour la propriété < voie > de la classe < AdressePostal > */
    public void setVoie(String p_voie)
    {
        this.voie = p_voie;
    }

    /* Mets à jour la propriété < ville > de la classe < AdressePostal > */
    public void setVille(String p_ville)
    {
        this.ville = p_ville;
    }

    /* Mets à jour la propriété < codePostal > de la classe < AdressePostal > */
    public void setCodePostal(int p_codePostal)
    {
        this.codePostal = p_codePostal;
    }
}
