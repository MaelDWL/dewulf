public class Voyageur_Class
{
	protected String Nom;

	/* CONSTRUCTOR */
	public Voyageur_Class(String p_Nom)
	{
		Nom = p_Nom;
	}

	/* SPECIFIC CLASS METHOD */
	public void AfficherVoyageur()
	{
		System.out.println("__________________________________");
		System.out.println("#       INFORMATION VOYAGEUR     #");
		System.out.println("__________________________________\n");
		System.out.println("Nom     : " + Nom + "\n");
	}
}
