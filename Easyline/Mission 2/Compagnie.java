import java.util.Scanner;
public class Compagnie {
    
    public String nom;
    public int code;
    public double ca;
    public String couleurPrincipaleLogo;
    public String couleurSecondaireLogo;


    public Compagnie(){

    }
    public Compagnie(String nomCompagnie){
        this.nom=nomCompagnie;
    }

    public Compagnie(String nomCompagnie, String couleurPrincipaleLogo, String couleurSecondaireLogo){
        this.nom=nomCompagnie;
        this.couleurP=couleurPrincipaleLogo;
        this.couleurPS=couleurSecondaireLogo;
    }
        
    public Compagnie(String nomCompagnie, String couleurPrincipaleLogo, String couleurSecondaireLogo, int code){
        this.nom=nomCompagnie;
        this.couleurP=couleurPrincipaleLogo;
        this.couleurPS=couleurSecondaireLogo;
        this.code=code;
    }


    public void afficher(){
        System.out.println(this.nom);
    }

    public void afficher1(){
        System.out.println("Nom de Compagnie : " + this.nom);
        System.out.println("Code de Compagnie : " + this.code);
    }

    public void afficher2(){
        System.out.println("Nom de Compagnie : " + this.nom);
        System.out.println("Code de Compagnie : " + this.code);
        System.out.println("Couleur de Compagnie : " + this.couleurPrincipaleLogo);
    }

    public String getNom(){ 
        return this.nom;
    }
    public void setNom(String nom){
        this.nom = nom;
    }


    public int getCode(){ 
        return this.code;
    }
    public void setCode(int code){ 
        this.code = code;
    }


    public double getCa(){ 
        return this.ca;
    }
    public void setCa(double ca){ 
        this.ca = ca;
    }
    public String getCouleur_principale(){ 
        return this.couleur_principale;
    }
    public void setCouleur_principale(String couleur_prinpale){ 
        this.couleur_principale = couleur_prinpale;
    }


    public String getCouleur_secondaire(){ 
        return this.couleur_secondaire + "second"; 
    }
    public void setCouleur_secondaire(String couleur_secondaire){ 
        this.couleur_secondaire = couleur_secondaire;
    }


    public static void main(String[] args){
        Compagnie maCompagnie1= new Compagnie();
        
        maCompagnie1.nom="zaza";
        maCompagnie1.afficher();
        
        Compagnie maCompagnie2= new Compagnie("zozo");
        maCompagnie2.afficher();
   
        Compagnie maCompagnie3= new Compagnie("EasyJune");
        maCompagnie3.afficher();
        
        Compagnie maCompagnie4= new Compagnie("Zizi");
        
    
        Compagnie maCompagnie5= new Compagnie();
        maCompagnie5.nom = "Zeze";

        Compagnie maCompagnie6= new Compagnie();
        maCompagnie6.nom = "Zuze";

        Compagnie maCompagnie7= new Compagnie();
        maCompagnie7.nom = "Zaze";

        maCompagnie1.code = 52;
        maCompagnie1.afficher1();
        
        maCompagnie1.couleurPrincipaleLogo = "Orange";
        maCompagnie1.couleurSecondaireLogo = "Vert";
        System.out.println("La couleur principale de " + maCompagnie1.nom + " est : " + maCompagnie1.couleurPrincipaleLogo);
        System.out.println("La couleur Secondaire de " + maCompagnie1.nom + " est : " + maCompagnie1.couleurSecondaireLogo);

        maCompagnie2.code = 63;
        maCompagnie2.afficher1();
    
        maCompagnie2.couleurPrincipaleLogo = "Jaune";
        maCompagnie2.couleurSecondaireLogo = "Rouge";
        System.out.println("La couleur principale de " + maCompagnie2.nom + " est : " + maCompagnie2.couleurPrincipaleLogo);
        System.out.println("La couleur Secondaire de " + maCompagnie2.nom + " est : " + maCompagnie2.couleurSecondaireLogo);

        maCompagnie3.code = 55;
        maCompagnie3.afficher1();
        
        maCompagnie3.couleurPrincipaleLogo = "Rouge";
        maCompagnie3.couleurSecondaireLogo = "Bleu";
        System.out.println("La couleur principale de " + maCompagnie3.nom + " est : " + maCompagnie3.couleurPrincipaleLogo);
        System.out.println("La couleur Secondaire de " + maCompagnie3.nom + " est : " + maCompagnie3.couleurSecondaireLogo);

        maCompagnie4.code = 22;
        maCompagnie4.afficher1();
        
        maCompagnie4.couleurPrincipaleLogo = "Rose";
        maCompagnie4.couleurSecondaireLogo = "Noir";
        System.out.println("La couleur principale de " + maCompagnie4.nom + " est : " + maCompagnie4.couleurPrincipaleLogo);
        System.out.println("La couleur Secondaire de " + maCompagnie4.nom + " est : " + maCompagnie4.couleurSecondaireLogo);

        maCompagnie5.code = 12;
        maCompagnie5.afficher1();
        
        maCompagnie5.couleurPrincipaleLogo = "Noir";
        maCompagnie5.couleurSecondaireLogo = "Violet";
        System.out.println("La couleur principale de " + maCompagnie5.nom + " est : " + maCompagnie5.couleurPrincipaleLogo);
        System.out.println("La couleur Secondaire de " + maCompagnie5.nom + " est : " + maCompagnie5.couleurSecondaireLogo);

        maCompagnie6.code = 78;
        maCompagnie6.afficher1();
        
        maCompagnie6.couleurPrincipaleLogo = "Mauve";
        maCompagnie6.couleurSecondaireLogo = "Rouge";
        System.out.println("La couleur principale de " + maCompagnie6.nom + " est : " + maCompagnie6.couleurPrincipaleLogo);
        System.out.println("La couleur Secondaire de " + maCompagnie6.nom + " est : " + maCompagnie6.couleurSecondaireLogo);

        maCompagnie7.code = 8;
        maCompagnie7.afficher1();

        maCompagnie7.couleurPrincipaleLogo = "Mauve";
        maCompagnie7.couleurSecondaireLogo = "ert";
        System.out.println("La couleur principale de " + maCompagnie7.nom + " est : " + maCompagnie7.couleurPrincipaleLogo);
        System.out.println("La couleur Secondaire de " + maCompagnie7.nom + " est : " + maCompagnie7.couleurSecondaireLogo);

        Scanner sc=new Scanner(System.in);
        System.out.println("Veuillez saisir le mot de passe "); /*Initialisation mdp code de la compagnie n6 */
        String mdp=sc.nextLine();
        if(mdp.equals("mdp")){ /*Demande de mdp */
            System.out.println("Mot de passe bon"); /*Afficher la compagnie de la véracité du mdp */
        }else{
            System.out.println("Mot de passe faux");
        }
        System.out.println("");
        
