# Mission : Wireshark et découvertes des protocoles

**MarkDown Dewulf Mael 14/04/2022**
---------------------------------------------------

### | Etape 1 - Installer Wireshark |

- [x] Télécharger ou récupérer le logiciel Wireshark (https://www.wireshark.org/) (Gnu Linux : # apt update et ensuite # apt install wireshark en tant que superutilisateur).

**Résumer en quelques phrases, l'utilité de ce logiciel et ce qu'il permettra de réaliser**

- Wireshark est un outil de capture et d’analyse de paquets. Il capture le trafic du réseau local et stocke les données ainsi obtenues pour permettre leur analyse hors ligne. Wireshark est capable de capturer le trafic Ethernet, Bluetooth. 

- [x] Installer Wireshark sur votre machine hôte (en cochant toutes les options utiles)

- [x] Démarrer l'application et essayer de la prendre en main

 **Quelles fonctionnalités de wireshark semblent intéressantes ?**

 Les fonctionalités qui sembles intéréssantes sont : 

 - La capture de paquets sur un réseau informatique 
 - La capture de paquets sur un USB
 - La capture de paquets sur un réseau téléphonique 
 - Voir les protocoles activés 
 - Analyser des fichier de donnée a partir de paquets qui ont déja été capturer 

 ![Déploiement mini-Protocole](B1_Systeme/img/diagram-Protocole.jpg)

 ----------------------------------------------------------------------------------------------------------------------------

 ### | Etape 2 - Analyser le trafic généré par un serveur http |
 
 **1.Que fait-on pour provoquer l'envoi d'une requête HTTP ? Préciser tous les détails pertinents.**
- Télécharger Wireshark.
- Ouvrez le navigateur Internet.
- Videz son cache de votre navigateur.
- Ouvrir Wireshark
- Cliquer sur « Capturer > Interfaces ». Une fenêtre contextuelle va s’afficher.
- Cliquer sur le bouton Démarrer pour capturer le trafic via cette interface.
- Visitez l’URL à partir de laquelle vous voulez capturer le trafic (Ici, un site en http non sécurisé)
- Revenir sur l'écran Wireshark et appuyer sur Ctrl + E pour arrêter la capture.

**2.Identifier avant chaque capture l'IP du client et l'IP du serveur.**

Source Address: 10.49.32.22

Destination Address: 23.77.197.147


|| **Le fichier** [CaptureHTTP](B1_Systeme/captureHTTP.pcapng) ||

**6.Filtrer les paquets HTTP échangés ?**
- Comment différencier les requêtes des réponses ?

--> On différencie les requêtes des réponses en regardant la source et la destination, avec la requête qui est envoyé par notre machine et la source qui sera donc notre iP. Une ligne de requête "GET" s'affiche lors d'une requete. 

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

--> Il y a eu 3 requêtes et 3 réponses.

-------------------------------------------------------------------------------------------------------------------------------
 ### |Etape 3 - Analyser le trafic généré par un serveur https |

**1.Que fait-on pour provoquer l'envoi d'une requête HTTP ? Préciser tous les détails pertinents.**

- Télécharger Wireshark.
- Ouvrez le navigateur Internet.
- Videz son cache de votre navigateur.
- Ouvrir Wireshark
- Cliquer sur « Capturer > Interfaces ». Une fenêtre contextuelle va s’afficher.
- Cliquer sur le bouton Démarrer pour capturer le trafic via cette interface.
- Visitez l’URL à partir de laquelle vous voulez capturer le trafic (Ici, un site en https comme Gitlab par exemple)
- Revenir sur l'écran Wireshark et appuyer sur Ctrl + E pour arrêter la capture.

**2.Identifier avant chaque capture l'IP du client et l'IP du serveur.**

Source Address: 10.49.32.22

Destination Address: 172.65.251.78

|| **Le fichier** [CaptureHTTPS](B1_Systeme/captureHTTPS.pcapng) ||

**6.Filtrer les paquets HTTP échangés ?**
- Comment différencier les requêtes des réponses ?

--> On différencie les requêtes des réponses en regardant la source et la destination, avec la requête qui est envoyé par notre machine et la source qui sera donc notre iP. Avant d'envoyer la requête HTTP GET, notre machine doit créer une connexion TCP avec le serveur en réalisant un échange de clés. Ensuite, Le serveur répond à la requête HTTP GET de notre machine par un message HTTP en ouvrant la page web demander.

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

--> Il y a eu 3 requêtes et 3 réponses.

**7.Quelles différences remarquez-vous avec le protocole HTTP ?**

--> La différence entre les deux est que le protocole HTTPS est plus sécurisé grâce au protocole TLS qui protége/sécurise l'envoie des paquets.

---------------------------------------------------------------------------------------------------------------------------------------------


### |Etape 4 - Analyser le trafic généré par un serveur ftp |

**1.Quel type de client faut-il installer pour vous y connecter ?** 

--> Pour s'y connecter, il faut installer un client ftp. [J'ai installé FileZilla]

**2.Que fait-on pour provoquer l'envoi d'une requête FTP ? Préciser tous les détails pertinents.**

- Aller sur un serveur ftp, 
- Se connecter en entrant un login et un mot de passe.
- Ouvrir File Zilla entrer les logs du site test DLP
- Ouvrir WireShark 
- Lancer la connexion puis capturer a partir du filtre ftp.

**3.Identifier avant chaque capture l'IP du client et l'IP du serveur.**

Source Address: 10.49.32.22

Destination Address: 35.163.228.146 

**7.Filtrer les paquets FTP échangés**

|| **Le fichier** [CaptureFTP](B1_Systeme/captureFTP.pcapng) ||

- Comment différencier les requêtes des réponses ?

--> On peux les diffférencier car les commandes sont sous forme de textes et les réponses sous forme de codes.

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

--> Il y a eu 3 requêtes et 3 réponses.

-------------------------------------------------------------------------------------------------------------

### |Etape 5 - Analyser le trafic généré par un serveur SSH |

**1.Quel type de client faut-il installer pour vous y connecter ?** 

--> Pour s'y connecter, il faut installer GitBash et se connecter sur notre espace AlwayData.

**2.Que fait-on pour provoquer l'envoi d'une requête SSH ? Préciser tous les détails pertinents.**

- Ouvrir Warshark
- Mettre sur pause l'analyse 
- Se positioner sur le filtre ssh 
- Lancer l'analyse 
- Aller sur GitBash
- Entrer la Commande pour se connecter a notre espace AlwaysData 
- Arreter la capture 

**3.Identifier avant chaque capture l'IP du client et l'IP du serveur.**

Source Address: 10.49.32.22

Destination Address: 185.31.40.87

**7.Filtrer les paquets FTP échangés**

|| **Le fichier** [CaptureSSH](B1_Systeme/captureSSH.pcapng) ||

- Comment différencier les requêtes des réponses ?

--> Pour différencier les requêtes des réponses on peux tout d'abord  regarder la source du premier échange qui sera la requête et la source du  deuxième échange qui sera la réponse. Ensuite, On peux aussi le voir quand le paquet vient du client ou du serveur.

- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 1. ?

--> Il y a eu 10 requetes et 10 réponses. 

**8.Que pouvez-vous noter de différent entre les serveurs SSH et FTP ?**

--> On peux noter que le serveur SSH est plus sécurisé dans les transferts de fichier par rapport au ftp qui n'est pas sécurisée.








