# Mission : Mutillidae

**MarkDown Dewulf Mael 16/06/2022**
---------------------------------------------------

### | Travail à faire 1 - S'enregistrer |

- [x] **Q-1 Créer un compte permettant de m'identifier sur la plate forme**
 ![Compte](B1_Systeme/img/InstallCpt.png)

- [x] **Q-2 Inspecter afin de découvrir les noms des champs login et mot de passe du formulaire d'authentification**
![Inspect](B1_Systeme/img/T1Q2.png)

- [x] **Q-3 Positionner le niveau de sécurité du code a 0 (Hosed)**
![Hos](B1_Systeme/img/T1Q3.png)

### | Travail à faire 2 - Découverte de la sensibilité SQLi |

- [x] **Q-1 Tester une détéction manuelle de la sensibilité SQLi** 
 ![Détéction](B1_Systeme/img/T2Q1.png)

### | Travail à faire 3 - A vous de jouer |

- [x] **Q-1 Extraction de données** 

**Défi 1:'or ('a' = 'a' ) or '**
![Test](B1_Systeme/img/T3Q1_.png)
![Test](B1_Systeme/img/T3Q1.png)

**' or username='admin**
![Test](B1_Systeme/img/T3Q2.png)

 **Défi 2:'or ('a' = 'a' ) or '** et **' or username='admin**
![Test](B1_Systeme/img/Deuxieme_defi.png)



### | Travail à faire 4  -Contres mesures |

- [x] **Q-1 Modifier le niveau de sécurité et vérifier que les tests d'exploitation des failles SQLi
échouent**

**Test avec sécurité niveau 5 avec ' or username='admin**
 ![Test](B1_Systeme/img/UIUIUI.png)


**Test avec sécurité niveau 5 avec 'or ('a' = 'a' ) or '**
![Test](B1_Systeme/img/UIOU.png)

- [x] **Q-2 Quel niveau de sécurité est nécessaire pour protéger contre cette attaque ?**
|| Le niveau minimum requis est le niveau de sécurité 1 ||

- [x] **Q-3 A ce niveau, quels sont les contrôles réalisés ?**
 
- $lEnableHTMLControls = TRUE;
- $lFormMethod = "GET";
- $lEnableJavaScriptValidation = TRUE;
- $lProtectAgainstMethodTampering = FALSE;
- $lEncodeOutput = FALSE;

- [x] **Q-4 Est-ce que le contrôle HTML aurait suffit à protéger contre cette injection SQL ?**
|| Oui, cela aurait suffit car le sql est injecté depuis l'html ||

- [x] **Q-4 Comment la validation javascript est-elle déclenchée ?**
|| C'est déclenchée si la variable lValidateInput = "TRUE" dans le fichier PHP_EOL ||

- [x] **Q-6 Quels sont les contrôles réalisés par la validation Javascript ?**
- La longueur de l'identifiant ne doit pas dépassé 15 caractères.
- Il faut que l'identifiant n'ai pas de caractères spéciaux.
