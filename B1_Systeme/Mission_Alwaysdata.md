# Mission : Déploiement sur un serveur distant avec AlwaysData    

**MarkDown Dewulf Mael 31/03/2022**
-------------------------------------

###  Etape 0 :  Créer un espace sur Alwaysdata 

- [x] Créer un compte sur Alwaysdata gratuit (100 Mo).

**Quels services sont offerts par Alwaysdata ?**

- Les service offerts par AlwaysData sont : l'hébergement de données comme: e-mails, des sites web, des médias, des bases de données, du support TLS. Avec un Accès Distant sur leurs serveurs.

**Quels services seront nécessaires pour déployer un site web (HTML, CSS, JS et PHP) ?**

- Les services nécessaires pour déployer un site web sont : la base de donnée, le domaine et l'accès distant.

**Quel est le nom de domaine choisi pour le déploiement de votre site ?**

- Le nom de domaine : **ssh-dewulf.alwaysdata.net**

###  Etape 1 : Activer SSH 

**Expliquer l'intérêt du protocole SSH. Sur quel port est-il actif par défaut ?**

- L'intérêt du Port SSH est que ca va permettre a deux machine d'établir une connexion directe et sécurisée au sein d'un réseau potentiellement non sécurisé, tel qu'Internet. Ceci est nécessaire pour que des tiers ne puissent pas accéder au flux de données et que les données sensibles ne tombent entre de mauvaises mains. C'est sur le port TCP 22, que le protocole SSH est actif et qu'il est mis par defaut.

**Quel autre protocole semble avoir les mêmes fonctionnalités ? Que fait SSH qui n'est pas possible avec le 2e ?**

- Le protocole samblant avoir les mêmes fonctionnalités est le protocole FTP. Le protocole SSH peut administrer à distance une machine. On parle ici de : créer des fichiers ou encore récupérer des lignes de commandes.

**Activer un accès au serveur via ce protocole. Quelles étapes sont nécessaires ?**

- Les étapes nécessaires pour activer un accès au serveur via ce protocole :

Aller sur AlwaysData > Aller sur Accès distant > Aller sur SSH > Activer la connexion par mot de passe et définir un mot de passe.

**Se connecter à votre espace dédié sur le serveur via ce protocole. Quelle est la ligne de commande nécessaire pour y arriver ?**

- La ligne de commande est: *ssh -v dewulf@ssh-dewulf.alwaysdata.net*

**Dans quel répertoire faut-il déposer vos fichiers du site si vous voulez le voir en ligne ?** (chemin complet sur le serveur)

- Pour déposer nos fichiers du site afin de le voir en ligne, il faut se connecter à notre espace dédié sur le serveur puis se rendre sur le répertoire : **/home/dewulf/www**

### Etape 2 : Copier notre contenu sur Alwaysdata 

**Quel est le chemin local absolu pour accéder à votre site ?**

- Le chemin local absolu: **/home/dewulf/www**

**Quel est le chemin absolu du répertoire dédié sur le serveur Alwaysdata ?**

- Le chemin absolu du répertoire: **/home/dewulf/**

**Les commandes scp et rsync peuvent être d'une grande aide à cette étape. Pourquoi ?**

- Les commandes scp et rsync sont une grande aide à cette étape, car il faut copier le contenu de notre site sur AlwaysData. La commande rsync est très utile pour la synchronisation des fichiers, et la commande scp est utile pour le transfert de fichiers.

**Quelle est la différence entre les deux commandes ?**

- La commande rsync a plus d'options que la commande scp. Elle peux copier localement et à distance les fichiers.Alors que la commande scp permet de copier seulement à distance les fichiers.

**Quelle est la commande complète pour ajouter les fichiers sauvegardés en local sur le serveur dédié ?**

- La commande complète : scp root@192.1.101:/dewulf/www/fichier/home/dewulf/data/

**Comment vérifier que l'ajout a bien été effectué ? Détailler la procédure et les résultats attendus.**

- Pour vérifier que l'ajout a bien été effectué, il faut aller dans le répertoire correspondant et faire la commande *ls* pour détailler ce qu'il y a dans le fichier. Si j'obtiens les fichiers de mon site alors, l'ajout a bien été effectué.

**Quelle URL permet de voir votre site en ligne ?**

- L'URL : **https://dewulf.alwaysdata.net/**
 
