## Exercices avec le terminal Windows10
### Dewulf Mael 
### 09/12/2021
---------------------------------
## Exercice 1 
##### Se déplacer vers le bon répertoire de travail 

C:\Users\Eleve> d:

D:> cd D:\Document

##### Créer le fichier ligne-commande

D:> cd D:\Document> mkdir ligne-commande

##### Créer le fichiers rep1, rep2, rep3

D:> cd D:\Document> cd ligne-commande

D:> cd D:\Document\ligne-commande> mkdir rep1

D:> cd D:\Document\ligne-commande> mkdir rep2

D:> cd D:\Document\ligne-commande> mkdir rep3


##### Créer les fihiers "fichier11.txt", "fichier12.txt" dans le rep2

D:> cd D:\Document\ligne-commande> cd rep1

D:> cd D:\Document\ligne-commande\rep1> type nul > fichier11.txt

D:> cd D:\Document\ligne-commande\rep1> type nul > fichier12.txt

##### Modifier les fichiers "fichier11.txt", "fichier12.txt"

D:> cd D:\Document\ligne-commande\rep1> echo MS-DOS >> fichier11.txt

D:> cd D:\Document\ligne-commande\rep1> echo Windows >> fichier12.txt

##### Créer le fichier "fichier22.txt" et le "rep21"

D:> cd D:\Document\ligne-commande\rep1> cd ../

D:> cd D:\Document\ligne-commande> cd rep2

D:> cd D:\Document\ligne-commande\rep2> type nul > fichier22.txt

D:> cd D:\Document\ligne-commande\rep2> mkdir rep21

##### Modifier le fichier "fichier22.txt"

D:> cd D:\Document\ligne-commande\rep1> cd ../

D:> cd D:\Document\ligne-commande> cd rep2

D:> cd D:\Document\ligne-commande\rep1> echo Linux >> fichier22.txt


##### Creér le fichiers "rep31" "rep32" dans le rep3

D:> cd D:\Document\ligne-commande\rep2> cd../

D:> cd D:\Document\ligne-commande> cd rep3

D:> cd D:\Document\ligne-commande\rep3> mkdir rep31

D:> cd D:\Document\ligne-commande\rep3> mkdir rep32

-----------------------------

## Exercice 2

##### Changer de partition

C:> d:

Afficher le contenu de la partition

D:> dir 

##### Créer un repertoire exo2

D:> cd Document

D:\Document> mkdir exo2

##### Se déplacer dans exo2

D:\Document> cd exo2

##### Créer le fichier "moi.txt"

D:\Document\exo2>type nul > moi.txt 

##### Afficher le message “Je suis en BTS SIO”

D:\Document\exo2>echo "Je suis en BTS SIO" >> moi.txt 

##### Ajouter au fichier moi.txt la ligne “Et j’aime ça !”

D:\Document\exo2>echo "Et j'aime ça" >> moi.txt 

##### Vous déplacer à la racine de la partition

D:\Document\exo2> ../../ 

--------------------------------

## Exercice 3

##### Créer un repertoire exo3

D:\> cd Document

D:\Document> mkdir exo3

##### Se déplacer dans exo3

D:\Document> cd exo3

##### Créer le fichier "moi.txt"

D:\Document\exo3> type nul > moi.txt

##### Ajouter le message "Je suis en BTS SIO"

D:\Document\exo3> echo Je suis en BTS SIO >> moi.txt

##### Rajouter le message "Et j'aime ca !"

D:\Document\exo3> echo Et j'aime ca ! >> moi.txt

##### Créer le fichier presidents.txt

D:\Document\exo3> type nul > presidents.txt

##### Rajouter "Francois Mitterand", "Jacques Chirac", "Georges Pompidou"

D:\Document\exo3> echo Francois Mitterand >> presidents.txt

D:\Document\exo3> echo Jacques Chirac >> presidents.txt

D:\Document\exo3> echo Georges Pompidou >> presidents.txt

##### Créer le fichier rio.txt

D:\Document\exo3> type nul > rio.txt

##### Rajouter "Rio de Janeiro est la deuxième plus grande ville du Brésil."

D:\Document\exo3> echo Rio de Janeiro est la deuxième plus grande ville du Brésil. >> rio.txt

##### En une seule commande

D:\Document\exo3> /

D:\Document\exo2>echo "Et j'aime ça" >> moi.txt 

##### Vous déplacer à la racine de la partition

D:\Document\exo2> ../../ 

-------------------------------------------
## Exercice 4

##### Créer le répertoire bin dans votre répertoire personnel :

D:\> cd Document

D:\Document> mkdir bin

##### Afficher la liste des variables d’environnement : 

D:\Document> cd bin

D:\Document\bin>> set 

##### Afficher le nom de l’utilisateur courant :

D:\Document\bin>> net user

USERNAME=User

#### Modifiez votre environnement pour :

##### Ajouter le répertoire bin créé précédemment à la variable PATH :

D:\Document\bin>>set PATH=%PATH%;C:\Document\bin

D:\>path

##### Créer l’alias dd qui lance la commande dir /p : 




