public class CoffeeOrderBuilder {
    private String type;
    private boolean lait;
    private boolean sucre;


    public CoffeeOrderBuilder(String type) {
        this.type = type;
    }

    public CoffeeOrderBuilder ajouterLait(boolean lait) {
        this.lait = lait;
        return this;
    }

    public CoffeeOrderBuilder ajouterSucre(boolean sucre) {
        this.sucre = sucre;
        return this;
    }



    public CoffeeOrder construire() {
        return new CoffeeOrder(type, lait, sucre);
    }
}
