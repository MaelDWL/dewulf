public class CoffeeOrder {
    private String type;
    private boolean lait;
    private boolean sucre;
    // autres extras

    public CoffeeOrder(String type, boolean lait, boolean sucre) {
        this.type = type;
        this.lait = lait;
        this.sucre = sucre;
        // initialisation des autres extras
    }

    // Getters et Setters
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isLait() {
        return lait;
    }

    public void setLait(boolean lait) {
        this.lait = lait;
    }

    public boolean isSucre() {
        return sucre;
    }

    public void setSucre(boolean sucre) {
        this.sucre = sucre;
    }

    @Override
    public String toString() {
        return "Commande Café{" +
                "type='" + type + '\'' +
                ", lait=" + lait +
                ", sucre=" + sucre +
                // autres extras
                '}';
    }
}
