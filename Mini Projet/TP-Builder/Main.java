public class Main {
    public static void main(String[] args) {
        // Création de commandes de café avec différentes configurations
        CoffeeOrder commande1 = new CoffeeOrderBuilder("Expresso").ajouterLait(false).ajouterSucre(false).construire();
        CoffeeOrder commande2 = new CoffeeOrderBuilder("Latte").ajouterLait(true).ajouterSucre(true).construire();

        // Affichage des détails de chaque commande
        System.out.println("Commande 1 : " + commande1);
        System.out.println("Commande 2 : " + commande2);

        // Tests supplémentaires
        // Commande avec lait et sans sucre
        CoffeeOrder commande3 = new CoffeeOrderBuilder("Cappuccino").ajouterLait(true).ajouterSucre(false).construire();
        System.out.println("Commande 3 : " + commande3);

        // Commande sans lait ni sucre
        CoffeeOrder commande4 = new CoffeeOrderBuilder("Americano").ajouterLait(false).ajouterSucre(false).construire();
        System.out.println("Commande 4 : " + commande4);
    }
}
