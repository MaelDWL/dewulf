package com.example.arbre;

public class ArbreBinaire {
    private Integer clef;
    private ArbreBinaire gauche;
    private ArbreBinaire droite;
    // Singleton pour représenter l'arbre vide
    private static final ArbreBinaire arbreVide = new ArbreBinaire();

    // Constructeur privé pour l'arbre vide
    private ArbreBinaire() {
        this.clef = null;
        this.gauche = null;
        this.droite = null;
    }

    // Getter et Setter pour la clef
    public Integer getClef() {
        return clef;
    }

    public void setClef(Integer clef) {
        this.clef = clef;
    }

    // Getter et Setter pour le sous-arbre gauche
    public ArbreBinaire getGauche() {
        return gauche;
    }

    public void setGauche(ArbreBinaire gauche) {
        this.gauche = gauche;
    }

    // Getter et Setter pour le sous-arbre droit
    public ArbreBinaire getDroite() {
        return droite;
    }

    public void setDroite(ArbreBinaire droite) {
        this.droite = droite;
    }

    // Méthode pour obtenir l'instance unique de l'arbre vide
    public static ArbreBinaire creer() {
        return arbreVide;
    }

    // Méthode pour vérifier si l'arbre est vide
    public boolean estVide() {
        return clef == null && gauche == null && droite == null;
    }

    public static void main(String[] args) {
        // Test de la création de l'arbre vide
        ArbreBinaire arbre = ArbreBinaire.creer();
        System.out.println("L'arbre est vide ? " + arbre.estVide());
    }

    // Méthode pour insérer une valeur dans l'arbre
    public void inserer(int valeur) {
        if (estVide()) {
            clef = valeur;
            gauche = creer();
            droite = creer();
        } else if (valeur < clef) {
            if (gauche.estVide()) {
                gauche = new ArbreBinaire();
            }
            gauche.inserer(valeur);
        } else if (valeur > clef) {
            if (droite.estVide()) {
                droite = new ArbreBinaire();
            }
            droite.inserer(valeur);
        }
        // Si la valeur est déjà présente, on ne fait rien
    }

    // Méthode pour afficher l'arbre (parcours infixe)
    public void afficher() {
        if (!estVide()) {
            gauche.afficher();
            System.out.println(clef);
            droite.afficher();
        }
    }

    // Méthode pour retourner la taille de l'arbre
    public int taille() {
        if (estVide()) {
            return 0;
        } else {
            return 1 + gauche.taille() + droite.taille();
        }
    }

    // Méthode pour rechercher une valeur dans l'arbre
    public boolean rechercher(int valeur) {
        if (estVide()) {
            return false;
        } else if (valeur == clef) {
            return true;
        } else if (valeur < clef) {
            return gauche.rechercher(valeur);
        } else {
            return droite.rechercher(valeur);
        }
    }



}

