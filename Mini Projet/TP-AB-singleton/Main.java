package com.example.arbre;

public class Main {
    public static void main(String[] args) {
        ArbreBinaire arbre = ArbreBinaire.creer();

        // Insertion de valeurs
        arbre.inserer(5);
        arbre.inserer(3);
        arbre.inserer(8);
        arbre.inserer(1);
        arbre.inserer(4);
        arbre.inserer(7);
        arbre.inserer(9);

        // Affichage de l'arbre
        System.out.println("Affichage de l'arbre :");
        arbre.afficher();

        // Affichage de la taille de l'arbre
        System.out.println("Taille de l'arbre : " + arbre.taille());

        // Recherche dans l'arbre
        int valeurRecherche = 4;
        System.out.println("La valeur " + valeurRecherche + " est présente dans l'arbre : " + arbre.rechercher(valeurRecherche));

        valeurRecherche = 6;
        System.out.println("La valeur " + valeurRecherche + " est présente dans l'arbre : " + arbre.rechercher(valeurRecherche));
    }
}
