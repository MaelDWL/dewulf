//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package dao;

import com.example.projetgreata.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import models.Compte;

public class CompteDAO {
    public CompteDAO() {
    }

    public Compte trouverparId(String login) {
        Compte compte = null;

        try {
            Connection conn = DBConnection.getConnection();

            try {
                PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM compte WHERE id = ?");
                pstmt.setString(1, String.valueOf(login));
                ResultSet rs = pstmt.executeQuery();
                if (rs.next()) {
                    compte = new Compte();
                    compte.setId(rs.getInt("id"));
                    compte.setLogin(rs.getString("login"));
                    compte.setMotDePasse(rs.getString("password"));
                }
            } catch (Throwable var7) {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Throwable var6) {
                        var7.addSuppressed(var6);
                    }
                }

                throw var7;
            }

            if (conn != null) {
                conn.close();
            }

            return compte;
        } catch (SQLException var8) {
            throw new RuntimeException("Erreur lors de la recherche de l'utilisateur par nom d'utilisateur.", var8);
        }
    }

    public boolean existe(String login, String mdp) {
        try {
            Connection conn = DBConnection.getConnection();

            boolean var6;
            try {
                PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM compte WHERE login = ? AND password = ?");
                pstmt.setString(1, login);
                pstmt.setString(2, mdp);
                ResultSet rs = pstmt.executeQuery();
                var6 = rs.next();
            } catch (Throwable var8) {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Throwable var7) {
                        var8.addSuppressed(var7);
                    }
                }

                throw var8;
            }

            if (conn != null) {
                conn.close();
            }

            return var6;
        } catch (SQLException var9) {
            throw new RuntimeException(var9);
        }
    }

    public void add(Compte c) {
        try {
            Connection conn = DBConnection.getConnection();

            try {
                PreparedStatement pstmt = conn.prepareStatement("INSERT INTO compte (login, password) VALUES (?, ?)");
                pstmt.setString(1, c.getLogin());
                pstmt.setString(2, c.getMotDePasse());
                pstmt.executeUpdate();
            } catch (Throwable var6) {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Throwable var5) {
                        var6.addSuppressed(var5);
                    }
                }

                throw var6;
            }

            if (conn != null) {
                conn.close();
            }

        } catch (SQLException var7) {
            throw new RuntimeException(var7);
        }
    }

    public void supprimer(int id) {
        try {
            Connection conn = DBConnection.getConnection();

            try {
                PreparedStatement pstmt = conn.prepareStatement("DELETE FROM compte WHERE id = ?");
                pstmt.setInt(1, id);
                pstmt.executeUpdate();
            } catch (Throwable var6) {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Throwable var5) {
                        var6.addSuppressed(var5);
                    }
                }

                throw var6;
            }

            if (conn != null) {
                conn.close();
            }

        } catch (SQLException var7) {
            throw new RuntimeException(var7);
        }
    }

    public void modifier(Compte compte) {
        try {
            Connection conn = DBConnection.getConnection();

            try {
                PreparedStatement pstmt = conn.prepareStatement("UPDATE compte SET login = ?, password = ? WHERE id = ?");
                pstmt.setString(1, compte.getLogin());
                pstmt.setString(2, compte.getMotDePasse());
                pstmt.setInt(3, Compte.getId());
                pstmt.executeUpdate();
            } catch (Throwable var6) {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Throwable var5) {
                        var6.addSuppressed(var5);
                    }
                }

                throw var6;
            }

            if (conn != null) {
                conn.close();
            }

        } catch (SQLException var7) {
            throw new RuntimeException(var7);
        }
    }

    public ArrayList<Compte> tousLesComptes() {
        ArrayList<Compte> comptes = new ArrayList();

        try {
            Connection conn = DBConnection.getConnection();

            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM compte");

                while(rs.next()) {
                    Compte compte = new Compte(rs.getInt("id"), rs.getString("login"), rs.getString("password"));
                    comptes.add(compte);
                }
            } catch (Throwable var7) {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Throwable var6) {
                        var7.addSuppressed(var6);
                    }
                }

                throw var7;
            }

            if (conn != null) {
                conn.close();
            }

            return comptes;
        } catch (SQLException var8) {
            throw new RuntimeException(var8);
        }
    }

    public Compte trouverParId(int id) {
        Compte compte = null;

        try {
            Connection conn = DBConnection.getConnection();

            try {
                PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM compte WHERE id = ?");
                pstmt.setInt(1, id);
                ResultSet rs = pstmt.executeQuery();
                if (rs.next()) {
                    compte = new Compte();
                    compte.setId(rs.getInt("id"));
                    compte.setLogin(rs.getString("login"));
                    compte.setMotDePasse(rs.getString("password"));
                }
            } catch (Throwable var7) {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (Throwable var6) {
                        var7.addSuppressed(var6);
                    }
                }

                throw var7;
            }

            if (conn != null) {
                conn.close();
            }

            return compte;
        } catch (SQLException var8) {
            throw new RuntimeException("Erreur lors de la recherche du compte par ID.", var8);
        }
    }

    public static void main(String[] args) {
        CompteDAO dao = new CompteDAO();
        ArrayList<Compte> comptes = dao.tousLesComptes();
        Iterator var3 = comptes.iterator();

        while(var3.hasNext()) {
            Compte comptess = (Compte)var3.next();
            System.out.println(comptess.getLogin());
        }

    }
}
