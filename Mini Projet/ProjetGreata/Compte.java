//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package models;

public class Compte {
    private static int id;
    private String login;
    private String motDePasse;

    public Compte() {
    }

    public Compte(int id, String login, String motDePasse) {
        Compte.id = id;
        this.login = login;
        this.motDePasse = motDePasse;
    }

    public static int getId() {
        return id;
    }

    public void setId(int id) {
        Compte.id = id;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return this.motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
}
