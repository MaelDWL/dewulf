//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.projetgreata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/projetgreata";
    private static final String DATABASE_USER = "root";
    private static final String DATABASE_PASSWORD = "root";

    public DBConnection() {
    }

    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetgreata", "root", "root");
            System.out.println("Connexion à la base de données réussie.");
            return connection;
        } catch (ClassNotFoundException var1) {
            System.out.println("Erreur : Driver JDBC non trouvé.");
            var1.printStackTrace();
            throw new RuntimeException("Erreur lors du chargement du driver JDBC.", var1);
        } catch (SQLException var2) {
            System.out.println("Erreur de connexion à la base de données.");
            var2.printStackTrace();
            throw new RuntimeException("Erreur de connexion à la base de données.", var2);
        }
    }
}
