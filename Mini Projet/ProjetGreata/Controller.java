//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.projetgreata;

import dao.CompteDAO;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import models.Compte;

@WebServlet(
    name = "Connexion",
    value = {"/Connexion"}
)
public class Controller extends HttpServlet {
    public Controller() {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = request.getParameter("login");
        String mdp = request.getParameter("password");
        CompteDAO dao = new CompteDAO();
        new Compte();
        if (dao.existe(login, mdp)) {
            response.sendRedirect("home.jsp");
        } else {
            response.sendRedirect("home.html?erreur=true");
        }

    }
}
