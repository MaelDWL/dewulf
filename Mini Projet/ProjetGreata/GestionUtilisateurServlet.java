//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package controller;

import dao.CompteDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import models.Compte;

@WebServlet({"/GestionUtilisateurServlet"})
public class GestionUtilisateurServlet extends HttpServlet {
    private CompteDAO compteDAO;

    public GestionUtilisateurServlet() {
    }

    public void init() throws ServletException {
        super.init();
        this.compteDAO = new CompteDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String login;
        if ("ajouter".equals(action)) {
            String login = request.getParameter("username");
            login = request.getParameter("password");
            Compte nouveauCompte = new Compte(0, login, login);
            this.compteDAO.add(nouveauCompte);
        } else if ("modifier".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            login = request.getParameter("username");
            String password = request.getParameter("password");
            Compte compte = new Compte(id, login, password);
            this.compteDAO.modifier(compte);
        }

        response.sendRedirect("home.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if ("supprimer".equals(action)) {
            int id = Integer.parseInt(request.getParameter("id"));
            this.compteDAO.supprimer(id);
            response.sendRedirect("home.jsp");
        }

    }
}
