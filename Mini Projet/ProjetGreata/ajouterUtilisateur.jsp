<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Ajouter un utilisateur</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f0f0f0;
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
      margin: 0;
    }
    form {
      background-color: #ffffff;
      padding: 40px;
      border-radius: 10px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
      width: 300px;
    }
    h2 {
      text-align: center;
      margin-bottom: 20px;
    }
    div {
      margin-bottom: 20px;
    }
    label {
      display: block;
      margin-bottom: 5px;
    }
    input[type="text"],
    input[type="password"] {
      width: 100%;
      padding: 10px;
      border: 1px solid #cccccc;
      border-radius: 5px;
      box-sizing: border-box;
    }
    button {
      width: 100%;
      padding: 10px;
      background-color: #28a745;
      color: white;
      border: none;
      border-radius: 5px;
      cursor: pointer;
    }
    button:hover {
      background-color: #218838;
    }
  </style>
</head>
<body>
<!-- Ajout du message de bienvenue avec le pseudo de l'utilisateur connecté -->
<h2>Bienvenue, <%= session.getAttribute("pseudoUtilisateur") %></h2>
<h2>Ajouter un nouvel utilisateur</h2>
<form action="GestionUtilisateurServlet" method="post">
  <input type="hidden" name="action" value="ajouter" />
  <div>
    <label for="username">Nom d'utilisateur:</label>
    <input type="text" id="username" name="username" required>
  </div>
  <div>
    <label for="password">Mot de passe:</label>
    <input type="password" id="password" name="password" required>
  </div>
  <div>
    <button type="submit">Ajouter</button>
  </div>
</form>
</body>
</html>
