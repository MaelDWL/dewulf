<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="models.Compte" %>
<%@ page import="dao.CompteDAO" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE html>
<html>
<head>
    <title>Accueil - Liste des utilisateurs</title>
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
        }
        h2 {
            color: #333;
        }
        table {
            border-collapse: collapse;
            width: 100%;
            margin-top: 20px;
            box-shadow: 0 4px 8px rgba(0,0,0,0.1);
        }
        th, td {
            text-align: left;
            padding: 12px;
            border-bottom: 1px solid #ddd;
        }
        th {
            background-color: #007bff;
            color: white;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        tr:hover {
            background-color: #ddd;
        }
        a, .add-user-link, .retour-link {
            color: white;
            padding: 8px 15px;
            text-decoration: none;
            border-radius: 5px;
            font-size: 16px;
            transition: background-color 0.3s ease;
        }
        .add-user-link, .retour-link {
            background-color: #28a745; /* Vert */
            margin-right: 10px;
        }
        .add-user-link:hover, .retour-link:hover {
            background-color: #218838;
        }
        a[href*="modifier"], a[href*="supprimer"] {
            margin-right: 5px;
            padding: 5px 10px;
            font-size: 14px;
        }
        a[href*="modifier"] {
            background-color: #007bff; /* Bleu */
        }
        a[href*="supprimer"] {
            background-color: #dc3545; /* Rouge */
        }
    </style>
</head>
<body>

<%

    CompteDAO Comptecompte = new CompteDAO();
    ArrayList<Compte> listeComptes = Comptecompte.tousLesComptes();
    String pseudo = (String) session.getAttribute("login");


%>



<table>

    <tr>
        <th>ID</th>
        <th>Login</th>
        <th>Mot de Passe</th>
        <th>Action</th>
    </tr>
    <%

        for (Compte comptes : listeComptes) {

            out.println("<tr>");
            out.println("<td>" + comptes.getId() + "</td>");
            out.println("<td>" + comptes.getLogin() + "</td>");
            out.println("<td>********</td>"); // Pour des raisons de sécurité, ne jamais afficher le vrai mot de passe
            out.println("<td><a href='modifierUtilisateur.jsp?id=" + comptes.getId() + "' style='background-color: #007bff;'>Modifier</a> | <a href='GestionUtilisateurServlet?action=supprimer&id=" + comptes.getId() + "' style='background-color: #dc3545;' onclick='return confirm(\"Êtes-vous sûr de vouloir supprimer cet utilisateur ?\");'>Supprimer</a></td>");
            out.println("</tr>");
        }
    %>
</table>
<div>

    <a href="ajouterUtilisateur.jsp" class="add-user-link">Ajouter un nouvel utilisateur</a>
    <a href="home.html" class="retour-link">Retour à l'accueil</a>
    <a href="login?action=logout" style="background-color: #dc3545; padding: 8px 15px; color: white; border-radius: 5px; font-size: 16px; text-decoration: none; margin-right: 10px;">Déconnexion</a>


</div>
</body>
</html>
