<%@ page import="dao.CompteDAO, models.Compte" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Modifier Utilisateur - <%= session.getAttribute("pseudoUtilisateur") %></title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        form {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            width: 300px;
        }
        h2, h3 {
            text-align: center;
            color: #333;
        }
        div {
            margin-bottom: 15px;
        }
        label {
            margin-bottom: 5px;
            display: block;
        }
        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-top: 5px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
        }
        button {
            width: 100%;
            padding: 10px;
            background-color: #007bff;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        button:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>

<%
    int id = Integer.parseInt(request.getParameter("id"));
    CompteDAO dao = new CompteDAO();
    Compte compte = dao.trouverParId(id);
    String pseudo = (String) session.getAttribute("login");
%>

<h2>Bienvenue, <%= compte.getLogin()%> !!</h2>

<h3>Modifier Utilisateur</h3>
<form action="GestionUtilisateurServlet" method="post">
    <input type="hidden" name="action" value="modifier"/>
    <input type="hidden" name="id" value="<%= compte.getId() %>"/>
    <div>
        <label for="username">Login:</label>
        <input type="text" id="username" name="username" value="<%= compte.getLogin() %>" required>
    </div>
    <div>
        <label for="password">Mot de Passe:</label>
        <input type="password" id="password" name="password" required placeholder="Entrez un nouveau mot de passe">
    </div>
    <button type="submit">Modifier</button>
</form>

</body>
</html>
