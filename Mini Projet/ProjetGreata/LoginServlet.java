//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.example.projetgreata;

import dao.CompteDAO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import models.Compte;

@WebServlet({"/login"})
public class LoginServlet extends HttpServlet {
    private CompteDAO compteDAO;

    public LoginServlet() {
    }

    public void init() throws ServletException {
        super.init();
        this.compteDAO = new CompteDAO();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("login");
        String password = request.getParameter("password");
        Compte cpt = new Compte();
        cpt.setLogin(username);
        cpt.setMotDePasse(password);

        RequestDispatcher dispatcher;
        try {
            CompteDAO dao = new CompteDAO();
            if (dao.existe(username, password)) {
                HttpSession session = request.getSession();
                session.setAttribute("username", username);
                RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
                dispatcher.forward(request, response);
            } else {
                System.out.println("no");
                dispatcher = request.getRequestDispatcher("index.jsp?error=true");
                dispatcher.forward(request, response);
            }
        } catch (Exception var9) {
            var9.printStackTrace();
            dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if ("logout".equals(action)) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }

            response.sendRedirect("index.jsp");
        }

    }
}
