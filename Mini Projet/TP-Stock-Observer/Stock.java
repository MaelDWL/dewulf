package org.example;

public class Stock {
    private String name; // Nom de l'action
    private float price; // Prix actuel de l'action

    // Constructeur pour initialiser les attributs de l'action
    public Stock(String name, float price) {
        this.name = name;
        this.price = price;
    }

    // Getter pour le nom de l'action
    public String getName() {
        return name;
    }

    // Setter pour le nom de l'action
    public void setName(String name) {
        this.name = name;
    }

    // Getter pour le prix de l'action
    public float getPrice() {
        return price;
    }

    // Setter pour le prix de l'action, avec mise à jour du prix
    public void setPrice(float price) {
        this.price = price;
    }
}

