package org.example;

import java.util.ArrayList;
import java.util.List;

public class StockMarket {
    private List<Observer> observers; // Liste des observateurs
    private List<Stock> stocks; // Liste des actions disponibles sur le marché

    // Constructeur
    public StockMarket() {
        observers = new ArrayList<>();
        stocks = new ArrayList<>();
    }

    // Méthode pour ajouter un observateur
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    // Méthode pour supprimer un observateur
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    // Méthode pour ajouter une action au marché
    public void addStock(Stock stock) {
        stocks.add(stock);
    }

    // Méthode pour mettre à jour le prix d'une action
    public void updateStockPrice(String stockName, float newPrice) {
        for (Stock stock : stocks) {
            if (stock.getName().equals(stockName)) {
                stock.setPrice(newPrice);
                notifyObservers(stock);
                break;
            }
        }
    }

    // Méthode privée pour notifier tous les observateurs du changement de prix
    private void notifyObservers(Stock stock) {
        for (Observer observer : observers) {
            observer.update(stock);
        }
    }
}

