package org.example;

public class Main {
    public static void main(String[] args) {
        StockMarket market = new StockMarket();

        // Création et ajout de stocks au marché
        Stock google = new Stock("Google", 2734.45f);
        Stock apple = new Stock("Apple", 150.75f);
        market.addStock(google);
        market.addStock(apple);

        // Création et ajout d'investisseurs (observateurs)
        Investor investor1 = new Investor("Investisseur 1");
        Investor investor2 = new Investor("Investisseur 2");
        market.addObserver(investor1);
        market.addObserver(investor2);

        // Mise à jour des prix des actions et notification des observateurs
        market.updateStockPrice("Google", 2750.00f);
        market.updateStockPrice("Apple", 152.00f);
    }
}
