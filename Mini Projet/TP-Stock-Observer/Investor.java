package org.example;

public class Investor implements Observer {
    private String name; // Nom de l'investisseur

    // Constructeur pour initialiser le nom de l'investisseur
    public Investor(String name) {
        this.name = name;
    }

    // Implémentation de la méthode update définie dans l'interface Observer
    @Override
    public void update(Stock stock) {
        // Affiche une notification pour cet investisseur lorsque le prix d'une action change
        System.out.println("Notification pour " + name + ": Le prix de l'action " + stock.getName() + " a été mis à jour à " + stock.getPrice());
    }

    // Getter pour le nom
    public String getName() {
        return name;
    }

    // Setter pour le nom
    public void setName(String name) {
        this.name = name;
    }
}
