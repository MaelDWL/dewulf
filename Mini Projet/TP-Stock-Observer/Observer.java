package org.example;

public interface Observer {
    // Méthode update qui sera appelée lorsqu'un changement se produit dans le sujet observé
    void update(Stock stock);
}

