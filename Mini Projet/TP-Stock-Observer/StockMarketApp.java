package org.example;

public class StockMarketApp {
    public static void main(String[] args) {
        // Création du marché boursier
        StockMarket market = new StockMarket();

        // Création et ajout des actions au marché
        Stock google = new Stock("Google", 1500.0f);
        Stock apple = new Stock("Apple", 2000.0f);
        market.addStock(google);
        market.addStock(apple);

        // Création et ajout des investisseurs au marché boursier comme observateurs
        Investor investor1 = new Investor("Investor 1");
        Investor investor2 = new Investor("Investor 2");
        Investor investor3 = new Investor("Investor 3");
        Investor investor4 = new Investor("Investor 4");

        market.addObserver(investor1);
        market.addObserver(investor2);
        market.addObserver(investor3);
        market.addObserver(investor4);

        // Simulation de changements de prix des actions et notification des observateurs
        System.out.println("\n-- Changement de prix de Google à 1520.0f --");
        market.updateStockPrice("Google", 1520.0f);

        System.out.println("\n-- Changement de prix de Apple à 2050.0f --");
        market.updateStockPrice("Apple", 2050.0f);

        // Autres simulations pour voir comment les investisseurs réagissent aux changements
        System.out.println("\n-- Changement de prix de Google à 1480.0f --");
        market.updateStockPrice("Google", 1480.0f);

        System.out.println("\n-- Changement de prix de Apple à 2100.0f --");
        market.updateStockPrice("Apple", 2100.0f);
    }
}



