
public class Acces {

	int id;
	String prenom;
	String login;
	String motdepasse;
	String statut;
	int age;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMotdepasse() {
		return motdepasse;
	}
	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Acces() {};
	public Acces(int id, String prenom, String login, String motdepasse, String statut, int age) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.login = login;
		this.motdepasse = motdepasse;
		this.statut = statut;
		this.age = age;
	}
}
