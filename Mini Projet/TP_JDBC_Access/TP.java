import java.sql.*;
public class TP {
	
	public static void Based( Acces a) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Chargement du driver");
			
			String dbName= "access";
			String login= "root";
			String motdepasse= "root";
			String strUrl = "jdbc:mysql://localhost:3306/" + dbName;
			
			Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
			System.out.println("ok connexion");
			String strInsert = "INSERT INTO acces (id, prenom, login, password, statut, age) VALUES (" +
				    a.getId() + ", '" +
				    a.getPrenom() + "', '" +
				    a.getLogin() + "', '" +
				    a.getMotdepasse() + "', '" +
				    a.getStatut() + "', " +
				    a.getAge() +
				    ")";
			System.out.println(strInsert);
			Statement stAddUser = conn.createStatement();
			stAddUser.executeUpdate(strInsert);
			
			String strQuery = "SELECT id, prenom, login, password, statut, age FROM acces";
            Statement stUsers = conn.createStatement();
            ResultSet rsUsers = stUsers.executeQuery(strQuery);

            while (rsUsers.next()) {
                int id = rsUsers.getInt("id");
                String prenom = rsUsers.getString("prenom");
                String loginValue = rsUsers.getString("login");
                String password = rsUsers.getString("password");
                String statut = rsUsers.getString("statut");
                int age = rsUsers.getInt("age");

                System.out.println("Id: " + id + ", Prénom: " + prenom + ", Login: " + loginValue + ", Password: " + password + ", Statut: " + statut + ", Âge: " + age);
            }

			conn.close();	
			
		}catch(ClassNotFoundException e) {
			System.out.println("je n'arrive pas a trouver le driver");
			
		}catch(SQLException e) {
			System.out.println("autre erreur");
			e.printStackTrace();
		}
	}
	public static void Show() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Chargement du driver");
			
			String dbName= "access";
			String login= "root";
			String motdepasse= "root";
			String strUrl = "jdbc:mysql://localhost:3306/" + dbName;
			
			Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
			String strQuery = "SELECT id, prenom, login, password, statut, age FROM acces";
            Statement stUsers = conn.createStatement();
            ResultSet rsUsers = stUsers.executeQuery(strQuery);

            while (rsUsers.next()) {
                int id = rsUsers.getInt("id");
                String prenom = rsUsers.getString("prenom");
                String loginValue = rsUsers.getString("login");
                String password = rsUsers.getString("password");
                String statut = rsUsers.getString("statut");
                int age = rsUsers.getInt("age");

                System.out.println("Id: " + id + ", Prénom: " + prenom + ", Login: " + loginValue + ", Password: " + password + ", Statut: " + statut + ", Âge: " + age);
            }

			conn.close();	
			
		}catch(ClassNotFoundException e) {
			System.out.println("je n'arrive pas a trouver le driver");
			
		}catch(SQLException e) {
			System.out.println("autre erreur");
			e.printStackTrace();
		}
	}
	public static void Delete() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Chargement du driver");
			
			String dbName= "access";
			String login= "root";
			String motdepasse= "root";
			String strUrl = "jdbc:mysql://localhost:3306/" + dbName;

            Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
            System.out.println("Connexion établie");

            // Demander à l'utilisateur de saisir l'identifiant de la ligne à supprimer
            java.util.Scanner scanner = new java.util.Scanner(System.in);
            System.out.print("Entrez l'identifiant de la ligne à supprimer : ");
            int idToDelete = scanner.nextInt();

            // Exécuter la requête DELETE
            String strDelete = "DELETE FROM acces WHERE id = ?";
            PreparedStatement stDelete = conn.prepareStatement(strDelete);
            stDelete.setInt(1, idToDelete);

            int rowsAffected = stDelete.executeUpdate();

            if (rowsAffected > 0) {
                System.out.println(rowsAffected + " ligne(s) supprimée(s) avec succès.");
            } else {
                System.out.println("Aucune ligne n'a été supprimée.");
            }

            stDelete.close();
            conn.close();
        } catch (ClassNotFoundException e) {
            System.out.println("Je n'arrive pas à trouver le driver");
        } catch (SQLException e) {
            System.out.println("Erreur SQL : " + e.getMessage());
        }
    }
	public static void modifyStatus(int idToModify, String newStatus) {
        try {
        	Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Chargement du driver");
			
			String dbName= "access";
			String login= "root";
			String motdepasse= "root";
			String strUrl = "jdbc:mysql://localhost:3306/" + dbName;

            Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
            System.out.println("Connexion établie");

            // Exécuter la requête UPDATE pour modifier le statut
            String strUpdate = "UPDATE acces SET statut = ? WHERE id = ?";
            PreparedStatement stUpdate = conn.prepareStatement(strUpdate);
            stUpdate.setString(1, newStatus);
            stUpdate.setInt(2, idToModify);

            int rowsAffected = stUpdate.executeUpdate();

            if (rowsAffected > 0) {
                System.out.println(rowsAffected + " ligne(s) modifiée(s) avec succès.");
            } else {
                System.out.println("Aucune ligne n'a été modifiée.");
            }

            stUpdate.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println("Erreur SQL : " + e.getMessage());
        }
        catch (ClassNotFoundException e) {
            System.out.println("Erreur SQL : " + e.getMessage());}
    }
	public static void main(String[] args) {
		//Object[] myTuple = {7, "Mael", "Maé", "azur","en ligne",20};
		//Acces a1 = new Acces((int) myTuple[0], (String) myTuple[1],(String) myTuple[2], (String) myTuple[3], (String) myTuple[4],(int) myTuple[5]);
		//TP.Based(a1);	;
		//TP.Delete();
		TP.modifyStatus(2,"Professeur");
	}
}
	
	//QUESTION 5 

