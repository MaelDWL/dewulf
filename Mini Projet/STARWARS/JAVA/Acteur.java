package starwars;

import java.util.ArrayList;

public class Acteur {
	private String nom;
	private String prenom;
	private ArrayList <Personnage> personnages;
	private Personnage[] duoPersonnages;
	
	public Acteur (){
		duoPersonnages = new Personnage[2];
	}
	
	public Acteur (String nom,String prenom,ArrayList <Personnage> personnages,Personnage personnage1, Personnage personnage2) {
		this.nom=nom;
		this.prenom=prenom;
		this.personnages=personnages;
		this.duoPersonnages = new Personnage[] {personnage1, personnage2};
	}
	
	
	
	
	public Personnage[] getDuoPersonnages() {
		return duoPersonnages;
	}

	public void setDuoPersonnages(Personnage[] duoPersonnages) {
		this.duoPersonnages = duoPersonnages;
	}

	public ArrayList<Personnage> getPersonnages() {
		return personnages;
	}

	public void setPersonnages(ArrayList<Personnage> personnages) {
		this.personnages = personnages;
	}

	public ArrayList<Personnage> getPersonnage() {
		return personnages;
	}

	public void setPersonnage(ArrayList<Personnage> Personnages) {
		this.personnages = Personnages;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public int nbPersonnages() {
	    int count = 0;
	    if (duoPersonnages != null) {
	        for (Personnage p : duoPersonnages) {
	            if (p != null) {
	                count++;
	            }
	        }
	    }
	    return count;
	}
	 
	 @Override
	    public String toString() {
	        String infoPersonnages = (personnages != null && !personnages.isEmpty()) ? personnages.toString() : "Aucun personnage";
	        return "Acteur{" +
	               "nom='" + nom + '\'' +
	               ", prenom='" + prenom + '\'' +
	               ", personnages=" + infoPersonnages +
	               '}';
	    }
	 
	 
















}



