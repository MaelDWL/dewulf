package starwars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Film {
		private String titre;
		private int anneeSortie;
		private int numepisode;
		private double cout;
		private int recette;
		private ArrayList <Acteur> acteurs;
		
		public Film () {
		}
		
		public Film (String titre,int anneeSortie,int numepisode,double cout,int recette,ArrayList <Acteur> acteurs) {
			this.titre=titre;
			this.anneeSortie=anneeSortie;
			this.numepisode = numepisode;
	        this.cout = cout;
	        this.recette = recette;
	        this.acteurs = acteurs;
		
		}

		
		
		public ArrayList<Acteur> getActeurs() {
			return acteurs;
		}

		public void setActeurs(ArrayList<Acteur> acteurs) {
			this.acteurs = acteurs;
		}

		public String getTitre() {
			return titre;
		}

		public void setTitre(String titre) {
			this.titre = titre;
		}

		public int getAnneeSortie() {
			return anneeSortie;
		}

		public void setAnneeSortie(int anneeSortie) {
			this.anneeSortie = anneeSortie;
		}

		public int getNumepisode() {
			return numepisode;
		}

		public void setNumepisode(int numepisode) {
			this.numepisode = numepisode;
		}

		public double getCout() {
			return cout;
		}

		public void setCout(double cout) {
			this.cout = cout;
		}

		public int getRecette() {
			return recette;
		}

		public void setRecette(int recette) {
			this.recette = recette;
		}
		
		public int nbActeurs() {
		    return acteurs.size();
		}
		
		public int nbPersonnages() {
		    int total = 0;
		    for (Acteur acteur : acteurs) {
		        total += acteur.nbPersonnages();
		    }
		    return total;
		}
		
		public Pair calculBenefice() {
		    double benefice = recette - cout;
		    return new Pair(benefice > 0, benefice);
		}
		
		public boolean isBefore(int annee) {
		    return this.anneeSortie < annee;
		}

		  public void tri() {
		        Collections.sort(acteurs, new Comparator<Acteur>() {
		            @Override
		            public int compare(Acteur acteur1, Acteur acteur2) {
		                return acteur1.getNom().compareTo(acteur2.getNom());
		            }
		        });
		    }

			@Override
		    public String toString() {
		        String infoActeurs = acteurs != null ? acteurs.toString() : "Aucun acteur";
		        return String.format(
		                "Film{titre='%s', anneeSortie=%d, numepisode=%d, cout=%.2f, recette=%d, acteurs=%s}",
		                titre, anneeSortie, numepisode, cout, recette, infoActeurs);
		    }	
		
		
		
		
}
		
		

		
