package starwars;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;

	public class main {
		 public static void main (String[] args)
		  	{
			 
			// Création des personnages
			Personnage lukeSkywalker = new Personnage("Luke", "Skywalker");
			Personnage leiaOrgana = new Personnage("Leia", "Organa");
			
			
			
			// Création des acteurs et assignation des personnages
			ArrayList<Personnage> personnagesMark = new ArrayList<>();
			personnagesMark.add(lukeSkywalker);
			Acteur markHamill = new Acteur();
			
			ArrayList<Personnage> personnagesCarrie = new ArrayList<>();
			personnagesCarrie.add(leiaOrgana);
			Acteur carrieFisher = new Acteur();
			
			// Création d'un nouvel acteur avec deux personnages
            Personnage personnage1 = new Personnage("NomPersonnage1", "PrenomPersonnage1");
            Personnage personnage2 = new Personnage("NomPersonnage2", "PrenomPersonnage2");
            
            Personnage personnageMark1 = new Personnage("Luke", "Skywalker");
        	Personnage personnageMark2 = new Personnage("NomAutrePersonnage", "RoleAutrePersonnage");
            
        	Acteur acteurAvecDeuxPersonnages = new Acteur("NomActeur", "PrenomActeur", new ArrayList<>(Arrays.asList(personnage1, personnage2)), personnage1, personnage2);

			
        	// Création de l'acteur Mark Hamill
            ArrayList<Personnage> personnagesMark1 = new ArrayList<>();
            personnagesMark1.add(lukeSkywalker);
            Acteur markHamill1 = new Acteur("Mark", "Hamill", personnagesMark1, lukeSkywalker, personnageMark2);
            Acteur carrieFisher1 = new Acteur("Carrie", "Fisher", personnagesCarrie, leiaOrgana, personnageMark2);
			 
            ArrayList<Acteur> acteursUnNouvelEspoir = new ArrayList<>();
			// Ajout des acteurs à la liste acteursUnNouvelEspoir
			 acteursUnNouvelEspoir.add(markHamill1);
			 acteursUnNouvelEspoir.add(carrieFisher1);
			 
			Film unNouvelEspoir = new Film("Star Wars, épisode IV : Un nouvel espoir", 1977, 4, 11000000, 775000000, acteursUnNouvelEspoir);
			
			
			
			Scanner scanner = new Scanner(System.in);
			System.out.println("Entrez le titre du film : ");
			String titre = scanner.nextLine();
			System.out.println("Entrez l'année de sortie : ");
			int anneeSortie = scanner.nextInt();
			System.out.println("Entrez le numéro de l'épisode : ");
			int numEpisode = scanner.nextInt();
			System.out.println("Entrez le coût de production : ");
			double cout = scanner.nextDouble();
			System.out.println("Entrez la recette du film : ");
			int recette = scanner.nextInt();
			
			// Utilisation des nouvelles méthodes
	        System.out.println("Nombre d'acteurs dans Un Nouvel Espoir: " + unNouvelEspoir.nbActeurs());
	        System.out.println("Nombre de personnages dans Un Nouvel Espoir: " + unNouvelEspoir.nbPersonnages());

	        Pair beneficeUnNouvelEspoir = unNouvelEspoir.calculBenefice();
	        System.out.println("Le film Un Nouvel Espoir est-il bénéficiaire ? " + beneficeUnNouvelEspoir.isBeneficiaire());
	        System.out.println("Montant du bénéfice/déficit : " + beneficeUnNouvelEspoir.getMontant());

	        int anneeAComparer = 1980;
	        System.out.println("Un Nouvel Espoir est-il sorti avant " + anneeAComparer + " ? " + unNouvelEspoir.isBefore(anneeAComparer));
			ArrayList<Acteur> acteursLaMenaceFantome = new ArrayList<>();
			// Ajoutez des acteurs à la liste acteursLaMenaceFantome
			Film laMenaceFantome = new Film(titre, anneeSortie, numEpisode, cout, recette, acteursLaMenaceFantome);
			
			// Affichage des acteurs triés pour Un Nouvel Espoir
	        System.out.println("Acteurs dans Un Nouvel Espoir après tri :");
	        for (Acteur acteur : unNouvelEspoir.getActeurs()) {
	            System.out.println(acteur.getNom()); 
			
			Personnage personnage = new Personnage("Skywalker", "Luke");
			System.out.println(personnage.toString());
			
			// Création d'une collection de films
	        ArrayList<Film> collectionFilms = new ArrayList<>();

	        // Ajout des films à la collection
	        collectionFilms.add(unNouvelEspoir);
	        collectionFilms.add(laMenaceFantome);
	        
	        // Création de la collection d'acteurs
	        ArrayList<Acteur> acteursDuFilm = new ArrayList<>();
	        acteursDuFilm.add(markHamill);
	        acteursDuFilm.add(carrieFisher1);
	        
	        // Trie les acteurs dans les films
	        unNouvelEspoir.tri();
	        laMenaceFantome.tri();
	        
	        // Ajout des acteurs à la liste
	        acteursUnNouvelEspoir.add(markHamill);
	        acteursUnNouvelEspoir.add(carrieFisher);
	        
	        // Affichage des films dans la collection
	        for (Film film : collectionFilms) {
	            System.out.println(film.toString());
	            
	        
            
	            
        }
}
	     
                        
	            
}    
}
	          
	            
	            
	            
	            
	            
	            
	            
	            
