package starwars;

public class Méchant extends Personnage {
    private boolean coteObscur;

    public Méchant(String nom, String prenom, boolean coteObscur) {
        super(nom, prenom); // Appel au constructeur de la classe parente
        this.coteObscur = coteObscur;
    }

    public boolean isCoteObscur() {
        return coteObscur;
    }

    public void setCoteObscur(boolean coteObscur) {
        this.coteObscur = coteObscur;
    }

    @Override
    public String toString() {
        return "Mechant{" +
                "nom='" + getNom() + '\'' +
                ", prenom='" + getPrenom() + '\'' +
                ", coteObscur=" + coteObscur +
                '}';
    }
}

	


