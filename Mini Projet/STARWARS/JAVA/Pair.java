package starwars;

public class Pair {
    private boolean isBeneficiaire;
    private double montant;

    public Pair(boolean isBeneficiaire, double montant) {
        this.isBeneficiaire = isBeneficiaire;
        this.montant = montant;
    }

    public boolean isBeneficiaire() {
        return isBeneficiaire;
    }

    public double getMontant() {
        return montant;
    }
}
