public class LightingControlSystemTest {
    public static void main(String[] args) {
        // Création d'une lampe
        Light light = new Light();

        // Création d'un panneau de contrôle d'éclairage
        LightControlPanel controlPanel = new LightControlPanel();

        // Ajout de la lampe au panneau de contrôle
        controlPanel.addLight(light);

        // Création des commandes pour allumer, éteindre et ajuster l'intensité lumineuse
        Command turnOnCommand = new TurnOnLightCommand(light);
        Command turnOffCommand = new TurnOffLightCommand(light);
        Command adjustBrightnessCommand = new AdjustBrightnessCommand(light, 50);

        // Exécution des commandes
        turnOnCommand.execute(); // Allumer la lampe
        turnOffCommand.execute(); // Éteindre la lampe
        adjustBrightnessCommand.execute(); // Ajuster l'intensité lumineuse

        // Vérification que le système fonctionne correctement
        System.out.println("Vérification de l'état de la lampe après les opérations : ");
        System.out.println("La lampe est-elle allumée ? " + light.isOn());
        System.out.println("Intensité lumineuse de la lampe : " + light.getIntensity() + "%");
    }
}
