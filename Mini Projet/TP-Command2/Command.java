// Command (Commande)
public interface Command {
    void execute(); // Méthode pour exécuter la commande
    void undo(); // Méthode pour annuler la commande précédente
}
