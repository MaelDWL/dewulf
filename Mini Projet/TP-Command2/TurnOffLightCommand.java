public class TurnOffLightCommand implements Command {
    private Light light; // Lampe à éteindre

    // Constructeur prenant en paramètre la lampe à éteindre
    public TurnOffLightCommand(Light light) {
        this.light = light;
    }

    // Implémentation de la méthode execute() pour éteindre la lampe
    @Override
    public void execute() {
        light.turnOff(); // Appel de la méthode turnOff() de la lampe
    }

    public void undo() {
    light.turnOn(); // Allumer la lampe pour annuler l'extinction précédente
}

}
