public class TurnOnLightCommand implements Command {
    private Light light; // Lampe à allumer

    // Constructeur prenant en paramètre la lampe à allumer
    public TurnOnLightCommand(Light light) {
        this.light = light;
    }

    // Implémentation de la méthode execute() pour allumer la lampe
    @Override
    public void execute() {
        light.turnOn(); // Appel de la méthode turnOn() de la lampe
    }

    public void undo() {
    light.turnOff(); // Éteindre la lampe pour annuler l'allumage précédent
    
    }

}
