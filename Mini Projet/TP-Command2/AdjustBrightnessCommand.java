public class AdjustBrightnessCommand implements Command {
    private Light light; // Lampe à ajuster
    private int intensity; // Intensité lumineuse souhaitée
    private int previousIntensity; // Intensité précédente pour pouvoir restaurer l'état    précédent lors de l'annulation

    // Constructeur prenant en paramètre la lampe à ajuster et l'intensité lumineuse souhaitée
    public AdjustBrightnessCommand(Light light, int intensity) {
        this.light = light;
        this.intensity = intensity;
        this.previousIntensity = light.getIntensity(); // Sauvegarder l'intensité précédente
    }

    // Implémentation de la méthode execute() pour régler l'intensité lumineuse de la lampe
    @Override
    public void execute() {
        light.setIntensity(intensity); // Appel de la méthode setIntensity() de la lampe avec l'intensité souhaitée
    }

    public void undo() {
    light.setIntensity(previousIntensity); // Restaurer l'intensité lumineuse précédente
}
}
