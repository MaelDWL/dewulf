import java.util.ArrayList;
import java.util.List;

public class LightControlPanel {
    private List<Light> lights; // Liste des lampes contrôlées par le panneau

    public LightControlPanel() {
        this.lights = new ArrayList<>(); // Initialisation de la liste des lampes
    }

    // Méthode pour ajouter une lampe au panneau de contrôle
    public void addLight(Light light) {
        lights.add(light);
        System.out.println("La lampe a été ajoutée au panneau de contrôle.");
    }

    // Méthode pour retirer une lampe du panneau de contrôle
    public void removeLight(Light light) {
        if (lights.remove(light)) {
            System.out.println("La lampe a été retirée du panneau de contrôle.");
        } else {
            System.out.println("Erreur : La lampe n'était pas présente dans le panneau de contrôle.");
        }
    }

    // Méthode pour allumer toutes les lampes du panneau de contrôle
    public void turnOnAllLights() {
        for (Light light : lights) {
            light.turnOn(); // Appel de la méthode d'allumage pour chaque lampe
        }
    }

    // Méthode pour éteindre toutes les lampes du panneau de contrôle
    public void turnOffAllLights() {
        for (Light light : lights) {
            light.turnOff(); // Appel de la méthode d'extinction pour chaque lampe
        }
    }

    // Méthode pour régler l'intensité lumineuse de toutes les lampes du panneau de contrôle
    public void setIntensityForAllLights(int intensity) {
        for (Light light : lights) {
            light.setIntensity(intensity); // Appel de la méthode de réglage d'intensité pour chaque lampe
        }
    }
}
