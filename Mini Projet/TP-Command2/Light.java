public class Light {
    private boolean isOn; // État de la lampe (allumée ou éteinte)
    private int intensity; // Intensité lumineuse de la lampe (en pourcentage)

    public Light() {
        this.isOn = false; // Au départ, la lampe est éteinte
        this.intensity = 0; // L'intensité lumineuse est à 0%
    }

    // Méthode pour allumer la lampe
    public void turnOn() {
        isOn = true;
        intensity = 100; // Lorsque la lampe est allumée, l'intensité est maximale (100%)
        System.out.println("La lampe est allumée.");
    }

    // Méthode pour éteindre la lampe
    public void turnOff() {
        isOn = false;
        intensity = 0; // Lorsque la lampe est éteinte, l'intensité est nulle (0%)
        System.out.println("La lampe est éteinte.");
    }

    // Méthode pour régler l'intensité lumineuse de la lampe
    public void setIntensity(int intensity) {
        if (intensity >= 0 && intensity <= 100) { // Vérification que l'intensité est dans la plage valide (0-100%)
            this.intensity = intensity;
            if (isOn) {
                System.out.println("L'intensité lumineuse de la lampe est réglée à " + intensity + "%.");
            } else {
                System.out.println("La lampe est éteinte. Impossible de régler l'intensité lumineuse.");
            }
        } else {
            System.out.println("Erreur : L'intensité lumineuse doit être comprise entre 0 et 100%.");
        }
    }
}
