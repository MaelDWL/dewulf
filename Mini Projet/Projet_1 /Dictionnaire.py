from collections import Counter
import re
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import requests

#_________________________________________________________________________________________________

# Question 1

def compter_occurrences_mots(texte):                    

    # Supprimer les caractères non alphabétiques et convertir le texte en minuscules
    mots = re.findall(r'\b\w+\b', texte.lower())

    # Compter les occurrences de chaque mot
    compte_mots = Counter(mots)

    # Trier par occurrence, puis alphabétiquement
    compte_mots_trie = dict(sorted(compte_mots.items(), key=lambda item: (-item[1], item[0])))
    return compte_mots_trie

# Exemple
exemple_texte = "Ceci est un texte de test, je suis un quoicoubaka. Mael est un enfant."
print(compter_occurrences_mots(exemple_texte))
print("\n")

#_________________________________________________________________________________________________

#Question 2

def filtrer_mots_parasites(compte_mots, mots_parasites):   

    #Filtre les mots parasites d'un dictionnaire de comptage de mots.
    {mot: compte for mot, compte in compte_mots.items() if mot not in mots_parasites}

# Exemple d'utilisation
exemple_compte_mots = {'ceci': 1, 'est': 2, 'un': 2, 'texte': 2, 'de': 1, 'test': 1, 'juste': 1, 'exemple': 1}
mots_parasites = ['un', 'de', 'la', 'le', 'les', 'Un']

compte_mots_filtre = filtrer_mots_parasites(exemple_compte_mots, mots_parasites)
print(compte_mots_filtre)
print("\n")

#_________________________________________________________________________________________________

#Question 3

def recuperer_mots_parasites(fichier):                     
    
    #Récupère les mots parasites à partir d'un fichier.
    mots_parasites = []
    try:
        with open(fichier, 'r') as file:
            for line in file:

                # Ajout chaque mot dans la liste, en supprimant les espaces blancs et les sauts de ligne
                mots_parasites.append(line.strip())
    except FileNotFoundError:
        print("Le fichier n'a pas été trouvé.")
    except Exception as e:
        print(f"Une erreur s'est produite: {e}")

    return mots_parasites

# Exemple d'utilisation
exemple_fichier = 'parasite.csv'
mots_parasites = recuperer_mots_parasites(exemple_fichier)
print(mots_parasites)
print("\n")

#_________________________________________________________________________________________________

#Question 5

def extraire_texte_html(html):
    
    #Extrait le texte d'une chaîne de caractères HTML.
    # Utilisation de BeautifulSoup pour analyser le HTML           
    soup = BeautifulSoup(html, 'html.parser')
    
    # Extraire le texte sans les balises HTML
    return soup.get_text()

# Exemple d'utilisation
exemple_html = "<html><head><title>Test Page</title></head><body><p>Ceci est un exemple de texte HTML.</p></body></html>"
texte_extrait = extraire_texte_html(exemple_html)
print(texte_extrait)
print("\n")

#_________________________________________________________________________________________________

#Question 6

def extraire_attributs_html_avec_bs(html, tag, attribut):                
    """
    Extrait les valeurs d'un attribut spécifique de toutes les occurrences d'une balise HTML donnée, en utilisant BeautifulSoup.

    Args:
    html (str): La chaîne de caractères HTML à analyser.
    tag (str): Le nom de la balise HTML à chercher.
    attribut (str): Le nom de l'attribut dont on veut extraire la valeur.

    Returns:
    list: Une liste des valeurs de l'attribut spécifié pour la balise donnée.
    """
    # Utiliser BeautifulSoup pour analyser le HTML
    soup = BeautifulSoup(html, 'html.parser')

    # Trouver toutes les occurrences de la balise
    balises = soup.find_all(tag)
    
    # Extraire la valeur de l'attribut pour chaque balise
    valeurs_attributs = [balise.get(attribut) for balise in balises if balise.has_attr(attribut)]
    return valeurs_attributs

# Exemple d'utilisation
exemple_html = '''
<html>
<head><title>Page de Test</title></head>
<body>
<img src="image1.jpg" alt="Image 1">
<img src="image2.jpg">
<a href="https://maeldewulf.fr" title="Un exemple">Lien</a>
</body>
</html>
'''

# Extraire les valeurs de l'attribut "alt" pour toutes les balises "img"
valeurs_alt = extraire_attributs_html_avec_bs(exemple_html, 'img', 'alt')
print("Valeurs de l'attribut 'alt' pour 'img':", valeurs_alt)
print("\n")

# Extraire les valeurs de l'attribut "href" pour toutes les balises "a"
valeurs_href = extraire_attributs_html_avec_bs(exemple_html, 'a', 'href')
print("Valeurs de l'attribut 'href' pour 'a':", valeurs_href)
print("\n")

#_________________________________________________________________________________________________

#question 8

def extraire_nom_domaine(url):                                
    """
    Extrait le nom de domaine d'une URL.

    Args:
    url (str): L'URL à partir de laquelle extraire le nom de domaine.

    Returns:
    str: Le nom de domaine de l'URL.
    """
    # Analyser l'URL et extraire le nom de domaine
    nom_domaine = urlparse(url).netloc
    return nom_domaine

# Exemple d'utilisation
exemple_url = "https://maeldewulf.fr/realisations_professionelles"
nom_domaine = extraire_nom_domaine(exemple_url)
print("Nom de domaine de l'URL donnée :", nom_domaine)
print("\n")

#_________________________________________________________________________________________________

# Question9

def classifier_urls_par_domaine(urls, domaine_specifie):         
    """
    Classifie des URLs en fonction de leur appartenance à un domaine spécifié.

    Args:
    urls (list): Liste d'URLs à classer.
    domaine_specifie (str): Le domaine spécifié pour la classification.

    Returns:
    dict: Un dictionnaire avec deux clés 'appartient' et 'nappartient_pas', contenant respectivement les URLs qui appartiennent ou non au domaine spécifié.
    """
    classification = {'appartient': [], 'nappartient_pas': []}
    for url in urls:
        # Extraire le nom de domaine de l'URL
        nom_domaine = urlparse(url).netloc

        # Classer l'URL en fonction de l'appartenance au domaine spécifié
        if domaine_specifie in nom_domaine:
            classification['appartient'].append(url)
        else:
            classification['nappartient_pas'].append(url)
    return classification

# Exemple d'utilisation
exemple_urls = [
    "https://maeldewulf.fr/"
    "https://maeldewulf.fr/realisations_professionelles",
]
domaine_specifie = "amazon.com"

classification = classifier_urls_par_domaine(exemple_urls, domaine_specifie)
print("Classification des URLs par domaine :", classification)
print("\n")

#_________________________________________________________________________________________________

# Question10

def recuperer_contenu_html(url):                       
    """
    Ouvre une page HTML depuis une URL et récupère son contenu HTML.

    Args:
    url (str): L'URL de la page à ouvrir.

    Returns:
    str: Le contenu HTML de la page, ou un message d'erreur si la récupération échoue.
    """
    try:
        # Envoyer une requête GET à l'URL
        reponse = requests.get(url)

        # Vérifier si la requête a réussi
        reponse.raise_for_status()

        # Retourner le contenu HTML de la page
        return reponse.text
    except requests.RequestException as e:
        # Retourner le message d'erreur en cas d'échec

        return f"Une erreur c'est produite lors de la récupération du contenu HTML: {e}"

# Exemple d'utilisation
exemple_url = "https://maeldewulf.fr"
contenu_html = recuperer_contenu_html(exemple_url)
print(contenu_html)
