public class TurnOnLightCommand implements Command {
    private Light light;
    private boolean wasOn; // État précédent de la lumière

    public TurnOnLightCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        wasOn = light.isOn(); // Supposons que Light a une méthode isOn()
        light.turnOn();
    }

    @Override
    public void undo() {
        if (!wasOn) {
            light.turnOff();
        }
    }
}
