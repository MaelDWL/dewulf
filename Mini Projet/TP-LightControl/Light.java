public class Light {
    private boolean on = false;
    private int brightness = 0; // Intensité lumineuse, de 0 à 100

    public void turnOn() {
        on = true;
        System.out.println("Lumière allumée.");
    }

    public void turnOff() {
        on = false;
        System.out.println("Lumière éteinte.");
    }

    public void adjustBrightness(int brightness) {
        this.brightness = brightness;
        System.out.println("Intensité réglée à " + brightness + ".");
    }

    // Méthode pour obtenir l'état allumé/éteint de la lumière
    public boolean isOn() {
        return on;
    }

    // Méthode pour obtenir l'intensité lumineuse actuelle
    public int getBrightness() {
        return brightness;
    }
}
