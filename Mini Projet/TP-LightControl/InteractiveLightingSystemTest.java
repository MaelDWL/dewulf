import java.util.Scanner;

public class InteractiveLightingSystemTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Configuration initiale du système
        Light livingRoomLight = new Light();
        Command turnOnLivingRoomLight = new TurnOnLightCommand(livingRoomLight);
        Command turnOffLivingRoomLight = new TurnOffLightCommand(livingRoomLight);

        Light kitchenLight = new Light();
        Command turnOnKitchenLight = new TurnOnLightCommand(kitchenLight);
        Command turnOffKitchenLight = new TurnOffLightCommand(kitchenLight);

        while (true) {
            System.out.println("\nSystème de contrôle d'éclairage intelligent:");
            System.out.println("1. Allumer la lumière du salon");
            System.out.println("2. Éteindre la lumière du salon");
            System.out.println("3. Allumer la lumière de la cuisine");
            System.out.println("4. Éteindre la lumière de la cuisine");
            System.out.println("5. Quitter");
            System.out.print("Sélectionnez une option: ");

            String input = scanner.nextLine();

            switch (input) {
                case "1":
                    turnOnLivingRoomLight.execute();
                    break;
                case "2":
                    turnOffLivingRoomLight.execute();
                    break;
                case "3":
                    turnOnKitchenLight.execute();
                    break;
                case "4":
                    turnOffKitchenLight.execute();
                    break;
                case "5":
                    System.out.println("Fermeture du programme...");
                    scanner.close();
                    return;
                default:
                    System.out.println("Option non valide, veuillez réessayer.");
                    break;
            }
        }
    }
}
