public class AdjustBrightnessCommand implements Command {
    private Light light;
    private int brightness;
    private int previousBrightness;

    public AdjustBrightnessCommand(Light light, int brightness) {
        this.light = light;
        this.brightness = brightness;
    }

    @Override
    public void execute() {
        previousBrightness = light.getBrightness(); // Supposons que Light a une méthode getBrightness()
        light.adjustBrightness(brightness);
    }

    @Override
    public void undo() {
        light.adjustBrightness(previousBrightness);
    }
}
