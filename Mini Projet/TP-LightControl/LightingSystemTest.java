public class LightingSystemTest {
    public static void main(String[] args) {
        // Créer des lumières
        Light livingRoomLight = new Light();
        Light kitchenLight = new Light();

        // Créer le panneau de contrôle et ajouter les lumières
        LightControlPanel controlPanel = new LightControlPanel();
        controlPanel.addLight(livingRoomLight);
        controlPanel.addLight(kitchenLight);

        // Créer les commandes
        Command livingRoomLightOn = new TurnOnLightCommand(livingRoomLight);
        Command kitchenLightOn = new TurnOnLightCommand(kitchenLight);
        Command livingRoomLightOff = new TurnOffLightCommand(livingRoomLight);
        Command adjustLivingRoomBrightness = new AdjustBrightnessCommand(livingRoomLight, 50);

        // Exécuter quelques commandes
        livingRoomLightOn.execute();
        kitchenLightOn.execute();
        adjustLivingRoomBrightness.execute();

        // Annuler les commandes
        System.out.println("Annulation des commandes...");
        adjustLivingRoomBrightness.undo();
        livingRoomLightOff.execute();

    }
}
