import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LightControlGUI {
    private JFrame frame;
    private JSlider brightnessSlider;
    private Light light;

    public LightControlGUI(Light light) {
        this.light = light;
        initializeUI();
    }

    private void initializeUI() {
        frame = new JFrame("Contrôle d'Éclairage Intelligent");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(400, 100));
        frame.setLayout(new FlowLayout());

        brightnessSlider = new JSlider(0, 100, light.getBrightness());
        brightnessSlider.setMajorTickSpacing(10);
        brightnessSlider.setPaintTicks(true);
        brightnessSlider.setPaintLabels(true);

        brightnessSlider.addChangeListener(e -> {
            int brightness = brightnessSlider.getValue();
            light.adjustBrightness(brightness);
            System.out.println("Intensité réglée à: " + brightness);
        });

        frame.add(brightnessSlider);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        Light livingRoomLight = new Light(); // Supposons que cette lumière est déjà intégrée dans votre système
        SwingUtilities.invokeLater(() -> new LightControlGUI(livingRoomLight));
    }
}
