public class TurnOffLightCommand implements Command {
    private Light light;
    private boolean wasOn; // État précédent de la lumière avant l'exécution de la commande

    public TurnOffLightCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        wasOn = light.isOn(); // Supposons que Light a une méthode isOn()
        light.turnOff();
    }

    @Override
    public void undo() {
        if (wasOn) {
            light.turnOn();
        }
    }
}
