public class ApplicationCafe {
    public static void main(String[] args) {
        Boisson cafe = new Cafe(); // Créez un café de base
        System.out.println(cafe.description() + " coûte " + cafe.cout() + "€");

        cafe = new  Lait(cafe);// Ajoute du lait
        System.out.println(cafe.description() + " coûte " + cafe.cout() + "€");

        cafe = new Sucre(cafe); // Ajoute du sucre
        System.out.println(cafe.description() + " coûte " + cafe.cout() + "€");

        cafe = new Caramel(cafe); // Ajoute du Caramel
        System.out.println(cafe.description() + " coûte " + cafe.cout() + "€");

        Boisson the = new The();
        System.out.println(the.description() + " coûte " + the.cout() + "€");

        the = new Miel(the); //Ajoute Miel
        System.out.println(the.description() + " coûte " + the.cout() + "€");
    }
}
