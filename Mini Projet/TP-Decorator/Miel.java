public class Miel extends DecorateurBoisson {
    public Miel(Boisson boisson) {
        super(boisson);
    }

    @Override
    public String description() {
        return boisson.description() + ", Miel";
    }

    @Override
    public double cout() {
        return boisson.cout() + 0.60; // Ajoute 0.20€ au coût de la boisson
    }
}