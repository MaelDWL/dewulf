import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Bienvenue dans notre système de café et thé personnalisable !");
        System.out.println("Veuillez choisir votre boisson de base: 1 pour Café, 2 pour Thé");
        String choixBoisson = scanner.nextLine().trim();

        Boisson boisson;
        if ("1".equals(choixBoisson)) {
            boisson = new Cafe(); // Café sélectionné
        } else if ("2".equals(choixBoisson)) {
            boisson = new The(); // Thé sélectionné
        } else {
            System.out.println("Choix non valide. Café sera sélectionné par défaut.");
            boisson = new Cafe(); // Choix par défaut
        }

        boolean ajouterPlus = true;
        System.out.println("Voulez-vous ajouter des ingrédients? (oui/non)");

        while (ajouterPlus && scanner.nextLine().trim().equalsIgnoreCase("oui")) {
            System.out.println("Quel ingrédient souhaitez-vous ajouter? (Lait/Sucre/Caramel/Miel) Tapez 'terminé' pour finir.");
            String choixIngredient = scanner.nextLine().trim().toLowerCase();

            switch (choixIngredient) {
                case "lait":
                    boisson = new Lait(boisson);
                    System.out.println("Lait ajouté.");
                    break;
                case "sucre":
                    boisson = new Sucre(boisson);
                    System.out.println("Sucre ajouté.");
                    break;
                case "caramel":
                    // Caramel pourrait être limité au café si vous le souhaitez
                    boisson = new Caramel(boisson);
                    System.out.println("Caramel ajouté.");
                    break;
                case "miel":
                    // Miel pourrait être limité au thé si vous le souhaitez
                    boisson = new Miel(boisson);
                    System.out.println("Miel ajouté.");
                    break;
                case "terminé":
                    ajouterPlus = false;
                    break;
                default:
                    System.out.println("Ingrédient non reconnu. Veuillez réessayer.");
                    break;
            }

            if (ajouterPlus) {
                System.out.println("Voulez-vous ajouter un autre ingrédient? (oui/non)");
            }
        }

        System.out.println("Votre commande: " + boisson.description() + ". Coût total: " + boisson.cout() + "€.");
        scanner.close();
    }
}

