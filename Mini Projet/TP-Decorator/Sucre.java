public class Sucre extends DecorateurBoisson {
    public Sucre(Boisson boisson) {
        super(boisson);
    }

    @Override
    public String description() {
        return boisson.description() + ", Sucre";
    }

    @Override
    public double cout() {
        return boisson.cout() + 0.20; // Ajoute 0.20€ au coût de la boisson
    }
}
