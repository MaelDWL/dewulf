public class Lait extends DecorateurBoisson {
    public Lait(Boisson boisson) {
        super(boisson);
    }

    @Override
    public String description() {
        return boisson.description() + ", Lait";
    }

    @Override
    public double cout() {
        return boisson.cout() + 0.50; // Ajoute 0.50€ au coût de la boisson
    }
}

