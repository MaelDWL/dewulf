public abstract class DecorateurBoisson implements Boisson {
    protected Boisson boisson; // Référence à une boisson que nous décorons

    public DecorateurBoisson(Boisson boisson) {
        this.boisson = boisson;
    }

    public abstract String description();
}

