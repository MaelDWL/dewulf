public class Caramel extends DecorateurBoisson {
    public Caramel(Boisson boisson) {
        super(boisson);
    }

    @Override
    public String description() {
        return boisson.description() + ", Caramel";
    }

    @Override
    public double cout() {
        return boisson.cout() + 0.70; // Ajoute 0.70€ au coût de la boisson
    }
}

