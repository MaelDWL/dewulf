public class Cafe implements Boisson {
    @Override
    public String description() {
        return "Café de base";
    }

    @Override
    public double cout() {
        return 1.99; // Le coût du café de base, vous pouvez ajuster ce prix.
    }
}

