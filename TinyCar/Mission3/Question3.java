import java.util.Scanner;

public class question3 {
public static void main (String [] args){

/* initialisation de l'ensemble des variables */

    String vhmarque;
    String vhmodele;
    String code;
    boolean elec;
    double prixht;
    double prixttc;
    double reduc;
    double remise = 0.1;
    double prixttcr;
    int qte = 1;
    int i = 1;

    Scanner sc = new Scanner(System.in);

    while (i <=3){
        System.out.println("Saisissez votre mot de passe ! ");       /* initialisation d'un code */
            code = sc.next();
            i = i + 1;

    if(code.equals("LOL")){


    System.out.println("Quel est la marque du véhicule ?");        /* demande à l'utilisateur la marque du véhicule */
    vhmarque = sc.nextLine();        /* mise en place de la variable "vhmarque" */
    sc.next();
   
   
    System.out.println("Quel est le modèle du véhicule ?");        /* demande à l'utilisateur le modèle du véhicule */
    vhmodele = sc.nextLine();        /* mise en place de la variable "vhmodele" */
    sc.next();

   
    System.out.println("Le véhicule est-il électrique ? tapez true (vrai) ou false (faux) ");       /* demande à l'utilisateur si le véhicule est électrique */
    elec = sc.nextBoolean();        /* mise en place de la variable "elec" */
   
   
    System.out.println("Quel est le prix HT du véhicule ?");        /* demande à l'utilisateur le prix HT du véhicule */
    prixht = sc.nextDouble();        /* mise en place de la variable "prixht" */
    
    double tva = 1.2;
    double tvaelec = 1.05;

    if (elec == true) { /* mise en place de la condition : si le véhicule est électrique, tva vaut 5% sinon tva vaut 20% */
        prixttc = prixht*(1+tvaelec)*qte;
        System.out.println(prixttc);
      }
    else{
        prixttc = prixht*(1+tva)*qte;
        System.out.println(prixttc);
    } 

    if (prixttc > 20000){ /* initialisation de la remise */
            
        reduc = prixttc * remise;
        prixttcr = prixttc - reduc;

        System.out.println("Le prix TTC de la " + vhmarque + " " + vhmodele + " est de : "+ prixttcr + " €(Après remise de 10% et " + tva*100 + "% de TVA)" );

    }else{

        System.out.println("Le prix TTC de la " + vhmarque + " " + vhmodele + " est de : " + prixttc +" € (" + tva*100 + "% de TVA)." );

    }
    
}
    /* mise en place d'un messsage d'erreur en cas de mauvais mot de passe */
     else{System.out.println("Mot de passe incorrect, Merci de réessayer !"); 
        }
    }
}
}
