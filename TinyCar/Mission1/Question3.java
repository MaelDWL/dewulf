import java.util.Scanner;
public class Question3 {
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Quel est le prix HT ?");        /* demande à l'utimisateur le prix HT*/
        double prixht = sc.nextDouble();        /*création de la variable "prixht" */
        double tauxtva = 1.6;       /* création de la variable "tauxtva" */
        double prixttc = prixht * tauxtva;      /* formule de calcul pour le prixttc en fonction du prixht et du tauxtva */
        System.out.println("Le prix TTC est de : "+ prixttc );      /* affichage du prix TTC*/

    }
}
