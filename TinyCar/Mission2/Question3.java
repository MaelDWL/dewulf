import java.util.Scanner;
public class Question3{
public static void main (String[] args){
  Scanner sc = new Scanner(System.in);
  String mdp = "Lux";                                          /* création du mod de passe*/
  System.out.println("Tapez le mot de passe");                 /*demande a l'utilisateur le mdp*/
  String mdp1 = sc.next();                                     /*création de la variable mdp1*/
  if ( mdp1.equals(mdp)){                                      /*si la variable corespond alors*/
    System.out.println("Bienvenue");                           /*message si le code est bon*/
    System.out.println("Quel est la marque du véhicule ?");    /*demande à l'utilisateur la marque du véhicule*/
    String vhmarque = sc.nextLine();                           /* création de la variable "vhmarque" */
    System.out.println("Quel est le modèle du véhicule ?");    /*demande à l'utilisateur le modèle du véhicule */
    String vhmodele = sc.nextLine();                           /*création de la variable "vhmodele */
    System.out.println("Véhicule électrique ? 0=faux 1=Vrai"); /* demande à l'utilisateur si le véhicule est éléctrique */
    int véléctrique = sc.nextInt();                          /*création de la variable "véléctrique */
    System.out.println("Quel est le prix HT du véhicule ?");  /* demande à l'utilisateur le prix HT du véhicule */
    double prixht = sc.nextDouble();                               /* création de la variable "prixht" */
    double tauxtva = 1.2;
    if (véléctrique == 1){                                            /* si voiture est vrai(donc élétrique)*/
        tauxtva = 1.05;                                     /* alors la tva sera de 5% sinon elle reste a 20%*/
    }
    double prixttc = prixht * tauxtva; /* formule de calcul pour le prixttc en fonction du prixht et du tauxtva */
    System.out.println("La marque du véhicule est : "+ vhmarque );      /* affichage de la marque du véhicule */
    System.out.println("Le modèle du véhicule est : "+ vhmodele );      /* affichage du modèle du véhicule */
    System.out.println("Le prix TTC est de : "+ prixttc );              /* affichage du prix TTC */
    if (prixttc > 20000) {                                                  /* si le prix est supérieur a 20 000  */
      double prixremise = prixttc * 0.9;                                  /* alors mettre remise a 10%  */
      System.out.println("Prix avec remise : " + prixremise );            /* calcul de la remise */
    }else{                             
      System.out.println("Voici le prix avec remise : " + prixttc);       /* affichage du prix avec remise */
    }
  }else{
    System.out.println("Mot de passe incorrect");                   /*sinon mettre "mdp incorect" et pas d'accès*/
  }
  }
}
