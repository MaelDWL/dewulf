import java.util.Scanner;
public class Question1 {
public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Quel est la marque du véhicule ?");  /* demande à l'utilisateur la marque du véhicule */
        String vhmarque = sc.nextLine();        /* création de la variable "vhmarque" */
        System.out.println("Quel est le modèle du véhicule ?");     /*demande à l'utilisateur le modèle du véhicule */
        String vhmodele = sc.nextLine();        /*création de la variable "vhmodele */
        System.out.println("Quel est le prix HT du véhicule ?");  /* demande à l'utilisateur le prix HT du véhicule */
        double prixht = sc.nextDouble();        /* création de la variable "prixht" */
        double tauxtva = 1.2;
        double prixttc = prixht * tauxtva; /* formule de calcul pour le prixttc en fonction du prixht et du tauxtva */
        System.out.println("La marque du véhicule est : "+ vhmarque );      /* affichage de la marque du véhicule */
        System.out.println("Le modèle du véhicule est : "+ vhmodele );      /* affichage du modèle du véhicule */
        System.out.println("Le prix TTC est de : "+ prixttc );      /* affichage du prix TTC */
    if (prixttc > 20000) {         /* si le prix est supérieur a 20 000  */
        double prixremise = prixttc * 0.9;  /* alors mettre remise a 10%  */
        System.out.println("Prix avec remise : " + prixremise );  /* calcul de la remise */
    }else{
        System.out.println("Voici le prix avec remise : " + prixttc);    /* affichage du prix avec remise */

    }
  }
