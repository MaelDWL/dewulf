# Dewulf 

**Dewulf Mael**

**Lycée**: Gustave Monod, Enghien les Bains 

**Formation:** STI2D, Option: Sin 

**Bac:** STI2D-2021, avec mention 

**Résidence:** Villaines-sous-bois

**Transport:** Train + Bus, Trajet: 1h 

**Pourquoi BTS SIO ?:** Formation la plus adapté est la plus intéressante selon moi suite a mon bac, j'ai toujours aimer le domaine de l'informatique. 

**Pourquoi l'ESIEE-IT ?:** Ecole dédouverte lors des portes ouvertes en 2020, c'est une école qui ma intérésser, proche de chez moi, et très moderne.

**Qui suis je comme étudiant ?:** Je suis un étudiant a la fois actif et sérieux en classe qui participe mais peux être parfois dissiper.

**Que dois-je savoir ?:** Rien de particulier. 

**Suis-je un adulte ?:** Bonsoir, Non. 

![IMAGE](logo_SKITTER.jpg_Detoure_Vrai_logo.png)

